/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import SplashScreen from 'react-native-splash-screen';

import { View, Image, Platform,DeviceEventEmitter } from 'react-native';
import { Provider } from 'react-redux';
import LaunchNavigator from 'react-native-launch-navigator';
import { configureStore } from "./src/Redux/Store/Store"
import SInfo from 'react-native-sensitive-info';
// import { Notification, NotificationOpen } from 'react-native-firebase';
// import firebase from 'react-native-firebase';
import firebase from 'react-native-firebase';

import Images from './src/Componants/Images';
import Constants from "./src/Componants/Constants";

import AuthLoadingScreen from './src/Screens/checkauth';
import LogInScreen from './src/Screens/login';
import ForgotPassword from './src/Screens/ForgotPassword';
import ResetPassword from './src/Screens/ResetPassword';
import GenerateOtp from './src/Screens/GenerateOtp';
import MyProfile from './src/Screens/Tabbar/More/MyProfile';
import WatchLocation from './src/Componants/WatchLocation';

// tabbar Screens
import HomePage from './src/Screens/Tabbar/Home/Home';
import JobRequest from './src/Screens/Tabbar/JobRequest/JobRequest';
import ScheduledOrder from './src/Screens/Tabbar/ScheduledOrder/ScheduledOrder';
import More from './src/Screens/Tabbar/More/More';
import OrderHistory from './src/Screens/Tabbar/More/OrderHistory';
import OrderDetail from './src/Screens/Tabbar/More/OrderDetail';
import ChangePassword from './src/Screens/Tabbar/More/ChangePassword';
import Notifications from './src/Screens/Tabbar/More/Notifications';


import NavigationService from './src/Componants/NavigationService';

const store = configureStore();

const HomeStack = createStackNavigator({
	// first: WatchLocation,
	Home: HomePage,
	OrderDetail: OrderDetail
}, {
	navigationOptions: ({ navigation }) => ({
		tabBarVisible: navigation.state.index < 1,
	})
});

const JobsRequestTab = createStackNavigator({
	JobRequest: JobRequest,
	OrderDetail: OrderDetail
}, {
	navigationOptions: ({ navigation }) => ({
		tabBarVisible: navigation.state.index < 1,
	})
});

const ScheduledOrderStack = createStackNavigator({
	// Home: SelectAddressScreen
	ScheduledOrder: ScheduledOrder,
	OrderDetail: OrderDetail
}, {
	navigationOptions: ({ navigation }) => ({
		tabBarVisible: navigation.state.index < 1,
	})
});

const MoreStack = createStackNavigator({
	// First: MyProfile,
	More: More,
	MyProfile: MyProfile,
	OrderHistory: OrderHistory,
	OrderDetail: OrderDetail,
	ChangePassword: ChangePassword,
	Notifications: Notifications,
	MyProfile: MyProfile,
}, {
	navigationOptions: ({ navigation }) => ({
		tabBarVisible: navigation.state.index < 1,
	})
});

const TabNavigator = createBottomTabNavigator({
	Home: {
		screen: HomeStack,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => (
				<View style={{ height: '100%', width: '100%' }}>
					{tintColor == 'black' ?
						<View style={{ borderTopColor: 'white', borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={Images.ImgHomeOff} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View> :
						<View style={{ borderTopColor: tintColor, borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={Images.ImgHomeOn} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View>
					}
				</View>
			)
		})
	},
	JobsRequest: {
		screen: JobsRequestTab,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => (
				<View style={{ height: '100%', width: '100%' }}>
					{tintColor == 'black' ?
						<View style={{ borderTopColor: 'white', borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={Images.ImgJobsOff} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View> :
						<View style={{ borderTopColor: tintColor, borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={Images.ImgJobsOn} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View>
					}
				</View>
			)
		})
	},
	ScheduledOrders: {
		screen: ScheduledOrderStack,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => (
				<View style={{ height: '100%', width: '100%' }}>
					{tintColor == 'black' ?
						<View style={{ borderTopColor: 'white', borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={Images.ImgScheduleOff} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View> :
						<View style={{ borderTopColor: tintColor, borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={Images.ImgScheduleOn} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View>
					}
				</View>
			)
		})
	},
	MoreTab: {
		screen: MoreStack,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => (
				<View style={{ height: '100%', width: '100%' }}>
					{tintColor == 'black' ?
						<View style={{ borderTopColor: 'white', borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={Images.ImgMenuOff} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View> :
						<View style={{ borderTopColor: tintColor, borderWidth: 2, height: '100%', width: '100%', borderColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
							<Image source={Images.ImgMenuOn} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
						</View>
					}
				</View>
			)
		})
	}
}, {
	// initialRouteName: "JobsRequest",
	tabBarOptions: {
		activeTintColor: Constants.green,
		inactiveTintColor: 'black',
		showLabel: false
	},
	lazy: true
});

const LoginStack = createStackNavigator({
	SignIn: LogInScreen,
	ForgotPassword: ForgotPassword,
	ResetPassword: ResetPassword,
	GenerateOtp: GenerateOtp,
});

let LoginNavigator = createAppContainer(
	createSwitchNavigator({
		// AuthLoading: AuthLoadingScreen,
		Login: LoginStack,
		Main: TabNavigator,
	}, { initialRouteName: "Login" })
);

let MainNavigator = createAppContainer(
	createSwitchNavigator({
		// AuthLoading: AuthLoadingScreen,
		Login: LoginStack,
		Main: TabNavigator,

	}, { initialRouteName: "Main" })
);

const RootNavigator = createAppContainer(
	createSwitchNavigator({
		AuthLoading: AuthLoadingScreen,
		Login: LoginStack,
		Main: TabNavigator,
	}, { initialRouteName: "AuthLoading" })
);

export default class App extends React.Component {
	state = {
		token: ''
	}

	componentWillMount = async () => {
		if (Platform.OS === "android") LaunchNavigator.setGoogleApiKey(Constants.GOOGLE_API_KEY);
		// this.createNotificationListeners();
		const token = await SInfo.getItem('token', Constants.sharedPreference);
		this.setState({ token: token })
		const user = await SInfo.getItem('userdetail', Constants.sharedPreference);
		Constants.user = user ? JSON.parse(user) : {};
		Constants.TOKEN = token ? token : "";
		SplashScreen.hide();
		const iosConfig = {
			clientId: '348600376683-ptbgl41hdb10ea20mb2crth12jq7m73e.apps.googleusercontent.com',
			appId: '1:348600376683:ios:973e08944857480e',
			apiKey: 'AIzaSyDSnWV4dyN6lNhIXKAjL82H83dFa9tBL-k',
			databaseURL: 'https://aapkidokan-4c311.firebaseio.com/',
			storageBucket: 'aapkidokan-4c311.appspot.com',
			messagingSenderId: '348600376683',
			projectId: Constants.projectId,
			// enable persistence by adding the below flag
			persistence: true,
		}

		const androidConfig = {
			clientId: '348600376683-hthe8o79427cusnnhm6ghmf9kq0ofrqe.apps.googleusercontent.com',
			appId: '1:348600376683:android:fc3e258bee39fb72',
			apiKey: 'AIzaSyDr0bnFXpvvpRajSxQYdIx5TkscFtVSbrM',
			databaseURL: 'https://aapkidokan-4c311.firebaseio.com/',
			storageBucket: 'aapkidokan-4c311.appspot.com',
			messagingSenderId: '348600376683',
			projectId: Constants.projectId,
			// enable persistence by adding the below flag
			persistence: true,
		}

		firebase.initializeApp(
			Platform.OS === 'ios' ? iosConfig : androidConfig,
			'taxiyefood'
		);
	}

	navigationFunction = () => {
		if (this.state.token) {
			console.log('we are here');
			console.log(Constants.TOKEN)
			return (<MainNavigator />)
		}
		else {
			console.log('we are here');
			return (<LoginNavigator />)
		}
	}

	showNotification(data) {
		console.log("ShowingNotification:" + JSON.stringify(data))
		console.log("NOTI_ID:" + Math.round((new Date()).getTime() / 1000).toString())
		const channel = new firebase.notifications.Android.Channel('Aapkidokan Driver', 'Aapkidokan Driver', firebase.notifications.Android.Importance.Max)
		firebase.notifications().android.createChannel(channel);
		const notification = new firebase.notifications.Notification()
			.setNotificationId(Math.round((new Date()).getTime() / 1000).toString())
			.setTitle(data.notificationTitle)
			.setBody(data.notificationBody)
			.android.setBigText(data.notificationBody)
			.setSound("default")
			.android.setAutoCancel(true)
			.setData(data)
			.android.setChannelId('Aapkidokan Driver')

		firebase.notifications().displayNotification(notification).then(() => console.log("Show")).catch(err => console.error("error " + err));
	}

	componentDidMount() {
    // this.getToken()
		const channel = new firebase.notifications.Android.Channel('ApkidokanDriver', 'ApkidokanDriver', firebase.notifications.Android.Importance.Max)
		firebase.notifications().android.createChannel(channel);
		firebase
			.notifications()
			.getInitialNotification()
			.then(res => {
				if (res) {
					console.log(res)
					let notification = res.notification;
					console.log("received initial notification " + JSON.stringify(notification.data));
					if (notification.data && notification.data.type) {
						notification.data["isNew"] = false
						this.saveNotification([notification.data])
						notification.data.type = parseInt(notification.data.type);
						this.handleNavigation(notification.data);
					}
				}
			});
		this.notificationOpenedListener = firebase
			.notifications()
			.onNotificationOpened((res) => {
				if (res) {
					console.log(res)
					let notification = res.notification;
					console.log("received opened notification " + JSON.stringify(notification.data));
					if (notification.data && notification.data.type) {
						notification.data["isNew"] = false
						this.saveNotification([notification.data])
						notification.data.type = parseInt(notification.data.type);
						this.handleNavigation(notification.data);
					}
				}
			});

		/*
		* Triggered when a particular notification has been received in foreground
		* */
		this.notificationListener = firebase.notifications().onNotification((res) => {
			console.log("NOTI_onNotification:", res)
			if (res && res.data && res.data.type) {
				console.log(res)
				let data = res.data;
				console.log("received on notification " + JSON.stringify(data));
				data["isNew"] = true
				this.saveNotification([data])
				if (data && data.notificationBody) {
					this.showNotification(data)
				}
			}
		});
	}

	showNotification(data) {
		console.log("ShowingNotification:" + JSON.stringify(data))
		const channel = new firebase.notifications.Android.Channel('ApkidokanDriver', 'ApkidokanDriver', firebase.notifications.Android.Importance.Max)
		firebase.notifications().android.createChannel(channel);
		const notification = new firebase.notifications.Notification()
			.setNotificationId("1")
			.setTitle(data.notificationTitle)
			.setBody(data.notificationBody)
			.android.setBigText(data.notificationBody)
			.setSound("default")
			.android.setAutoCancel(true)
			.setData(data)
			.android.setChannelId('ApkidokanDriver')

		firebase.notifications().displayNotification(notification).then(() => console.log("Show")).catch(err => console.log("Error:{", err));

	}

	saveNotification = async (data) => {
		if (!data || data.length == 0) {
			return
		}
		let notifications = await SInfo.getItem('notifications', Constants.sharedPreference);
		if (notifications) {
			// console.log("NOTI_1:" + notifications)
			let parsedData = JSON.parse(notifications)
			const filteredData = parsedData.filter(function (existedItem) {
				const itemData = existedItem.order_id;
				if (itemData.indexOf(data[0].order_id) > -1) {
					return itemData.indexOf(data[0].order_id) > -1;
				}
			});
			// console.log("FILTERED:"+JSON.stringify(filteredData))
			// if (Array.isArray(filteredData) && filteredData.length == 0) {
				const newData = [...parsedData, ...data]
				// console.log("NEW_NOTI:"+JSON.stringify(newData))
				setTimeout(() => {
					SInfo.setItem('notifications', JSON.stringify(newData), Constants.sharedPreference);
					DeviceEventEmitter.emit("ADD_NOTIFICATION")
					DeviceEventEmitter.emit("ORDER_NOTIFICATION")
					if(data && data.length>0 && data[0].orderStatus && data[0].orderStatus == 5){
						DeviceEventEmitter.emit("CANCEL_NOTIFICATION")
					}
					
				}, 1000);
			// }

		} else {
			// console.log("NOTI_2:")
			setTimeout(() => {
				SInfo.setItem('notifications', JSON.stringify(data), Constants.sharedPreference);
				DeviceEventEmitter.emit("ADD_NOTIFICATION")
				DeviceEventEmitter.emit("ORDER_NOTIFICATION")
				if(data && data.length>0 && data[0].orderStatus && data[0].orderStatus == 5){
					DeviceEventEmitter.emit("CANCEL_NOTIFICATION")
				}
			}, 1000);
		}

	}

	handleNavigation = (data) => {
		let routeName = "Home";
		switch (data.type) {
			case 1:
				routeName = "JobsRequest"
				break;
			default:
				break;
		}
		NavigationService.navigate(routeName);
		// DeviceEventEmitter.emit(Constants.LISTEN_NOTIFICATION, routeName);
	}

	render() {
		console.disableYellowBox = true;
		return (
			<Provider store={store}>
				<RootNavigator
					ref={navigatorRef => {
						NavigationService.setTopLevelNavigator(navigatorRef);
					}}
				/>
				<WatchLocation />
			</Provider>
		)
	}
}