import constants from './../Componants/Constants'
import SInfo from 'react-native-sensitive-info';
import moment from 'moment';
import NetInfo from "@react-native-community/netinfo";
import NavigationService from '../Componants/NavigationService';
import Constants from './../Componants/Constants';

async function callApi(urlString, methodType, body) {
	console.log("urlString:--" + urlString);
	console.log("body:--" + JSON.stringify(body));
	const netInfo = await NetInfo.fetch();
	if (!netInfo.isConnected) {
		return { singleStringMessage: "Internet is not available" }
	}
	console.log("Network connected? " + netInfo.isConnected);
	let token = await SInfo.getItem('token', constants.sharedPreference);
	console.log("token:--" + (token));
	let header = {
		"Accept": "application/json",
		"Content-Type": "application/json",
		"Authorization": token ? "Bearer " + token : null,
		// token: Constants.dailyToken,
		// agency_Id: Constants.agencyId
	}
	console.log("HEADER:-",JSON.stringify(header))
	return fetch(urlString, {
		method: methodType,
		headers: header,
		body: (methodType == "POST" || methodType == "PUT") ? JSON.stringify(body) : null
	}).then(response => {
		return response.json()
	})
		.then(async (response) => {
			if (response.error && response.error == 401) {
				if (Constants.timerId) {
					clearInterval(Constants.timerId);
				}
				NavigationService.navigate("Login");
				await SInfo.deleteItem("token", Constants.sharedPreference);
				await SInfo.deleteItem("userdetail", Constants.sharedPreference);
				await SInfo.deleteItem('notifications', Constants.sharedPreference);
				return { singleStringMessage: "You have been loged out", code: { code: 401 } };
			} else if ((response.code && response.code == 200) || response.singleStringMessage) {
				// console.log(response.json())
				return response
			} else {
				throw new Error(" status code " + (response.code || response.error))
			}
		})
		.catch((error) => {
			throw error
		})
}

export const login = async (data) => {
	data.fcm_token = constants.FCM_TOKEN;
	return callApi(constants.url + 'auth/login', "POST", data)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const logOut = () => {
	return callApi(constants.url + 'auth/logout', "GET")
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const homeData = (body, pageNo) => {
	let url = constants.url + 'order?';
	if (body.status) {
		url += 'status=' + body.status + "&pageNo=" + pageNo + "&perPage=500";
	} else if (body.from_date && body.to_date) {
		url += "&from_date=" + body.from_date + "&to_date=" + body.to_date + "&name=created_at&sortType=-1&pageNo=" + pageNo + "&perPage=10";
	}
	return callApi(url, "GET")
		.then((res) => {
			console.log(JSON.stringify(res));
			if (res.data && res.data.orders) {
				for (let i = 0; i < res.data.orders.length; i++) {
					res.data.orders[i].customer.picture
					if (res.data.orders[i].customer.picture && !res.data.orders[i].customer.picture.startsWith("http")) {
						res.data.orders[i].customer.picture = constants.imageBaseURL + res.data.orders[i].customer.picture;
					}
					switch (res.data.orders[i].status) {
						case 1:
							res.data.orders[i].orderStatus = "Placed";
							break;
						case 2:
							res.data.orders[i].orderStatus = "Picked Up";
							break;
						case 3:
							res.data.orders[i].orderStatus = "Delivered";
							break;
						case 4:
							res.data.orders[i].orderStatus = "UnDelivered";
							break;
						case 5:
							res.data.orders[i].orderStatus = "Cancelled";
							break;
					}
				}
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const changeOrderStatus = (id, data) => {
	return callApi(constants.url + 'order/status-update/' + id, "PUT", data)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const changePassword = (data) => {
	return callApi(constants.url + 'auth/change-password/', "POST", data)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getOrdersToAccept = (pageNo) => {
	return callApi(constants.url + 'order/unassigned?name=created_at&sortType=-1&pageNo=' + pageNo, "GET")
		.then((res) => {
			if (res.data && res.data.orders) {
				for (let i = 0; i < res.data.orders.length; i++) {
					res.data.orders[i].order_date = moment(res.data.orders[i].deliver_start_time).format("D MMMM, YYYY");
					// res.data.orders[i].customer = { picture: "https://zdnet3.cbsistatic.com/hub/i/r/2018/08/14/e12bd7e8-6fd7-46a1-b9b3-dc015f001216/resize/770xauto/9c22e91bc78ace64efc8c9c62db65637/sainsburys-smartshop-scan-pay-go-trial-customers-scanning-their-shopping-with-their-smartphone-fo.jpg" };
					if (res.data.orders[i].customer.picture && !res.data.orders[i].customer.picture.startsWith("http")) {
						res.data.orders[i].customer.picture = constants.imageBaseURL + res.data.orders[i].customer.picture;
					}
				}
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getScheduledOrders = () => {
	return callApi(constants.url + 'order/scheduled', "GET")
		.then((res) => {
			if (res && res.data && res.data.orders) {
				for (let i = 0; i < res.data.orders.length; i++) {
					res.data.orders[i].order_date = moment(res.data.orders[i].deliver_start_time).format("D MMMM, YYYY");
					if (res.data.orders[i].customer.picture && !res.data.orders[i].customer.picture.startsWith("http")) {
						res.data.orders[i].customer.picture = constants.imageBaseURL + res.data.orders[i].customer.picture;
					}
				}
				res.data.orders = res.data.orders.sort((a, b) => {
					let comparison = 0;
					if (new Date(a.order_date) > new Date(b.order_date)) {
						comparison = 1;
					} else if (a.order_date < b.order_date) {
						comparison = -1;
					}
					return comparison;
				});
			}
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const acceptOrder = async (id) => {
	return callApi(constants.url + 'order/accept/' + id, "PUT")
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const forgotPassword = async (data) => {
	return callApi(constants.url + 'auth/forget-password', "POST", data)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const verifyOtp = async (data) => {
	return callApi(constants.url + 'auth/verify-otp', "POST", data)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const resetPassword = async (data) => {
	return callApi(constants.url + 'auth/reset-password', "POST", data)
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const updateStatus = async (status) => {
	return callApi(constants.url + 'user/status-change?is_online=' + status, "GET")
		.then((res) => {
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const getNotificationApi = (data) => {
	// const url = constants.url + 'notification?limit=' + data.limit + '&offset=' + data.offset
	const url = constants.url + 'notification'
	console.log("API CALL IS : GET NOTIFICATION API")
	console.log("URL: ", url)
	console.log("data: ", JSON.stringify(data))
	return callApi(url, "GET")
		.then((res) => {
			console.log("RESPONSE IS")
			console.log(res)
			console.log(JSON.stringify(res))
			return res;
		}).catch((error) => {
			console.log(error);
		});
}

export const deleteNotificationsApi = (data) => {
	// const url = constants.url + 'notification?limit=' + data.limit + '&offset=' + data.offset
	const url = constants.url + 'notification'
	console.log("API CALL IS : GET NOTIFICATION API")
	console.log("URL: ", url)
	console.log("data: ", JSON.stringify(data))
	return callApi(url, "DELETE")
		.then((res) => {
			console.log("RESPONSE IS")
			console.log(res)
			console.log(JSON.stringify(res))
			return res;
		}).catch((error) => {
			console.log(error);
		});
}