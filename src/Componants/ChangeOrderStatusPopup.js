import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image, Modal, TextInput } from 'react-native';
import Images from './Images'
import Constants from './Constants'
import Fonts from './Fonts'
import Spinner from 'react-native-loading-spinner-overlay';

export default class ChangeOrderStatusPopup extends Component {
	state = {
		reason: "",
		reasonIndex: 1,
	}

	componentDidMount() {
		console.log("Modal created");
	}

	render() {
		let { modalVisible, modalLoading, markCompleteFunction, markIncompleteFunction, closeModel,
			markcomplete, modalData, visibilityfunction, submitReasonFunction,closeUndeliverOrderModal } = this.props;
		let statusMark = "";
		switch (modalData.status) {
			case 1:
				statusMark += "MARK AS PICKED UP";
				break;
			case 2:
				statusMark = "READY TO DELIVER";
				break;
			case 6:
				statusMark = "MARK AS COMPLETED";
				break;
		}
		return (
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					Alert.alert('Modal has been closed.');
				}}
			>
				<Spinner size="large"
					visible={modalLoading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				{markcomplete ?
					<View style={styles.container}>
						<View style={{ height: 170, width: '95%', flexDirection: 'column', backgroundColor: 'white', borderRadius: 10 }}>
							<View style={{ height: '18%', width: '100%', flexDirection: 'row', marginBottom: 10 }}>
								<View style={{ height: '100%', width: '80%', flex: 1, flexDirection: 'row' }}>
									<View style={{ marginTop: 18, marginLeft: 15, flexDirection: 'row', }}>
										<Text style={{ color: Constants.black, fontSize: 16, fontFamily: Fonts.semiBold, width: '80%', marginTop: -7 }}>Change Order status</Text>
										<TouchableOpacity style={{ width: '20%', alignItems: 'flex-end', height: "100%", justifyContent: "center" }} onPress={() => closeModel()}>
											<Image source={Images.ImgClose} style={styles.closeIcon}></Image>
										</TouchableOpacity>
									</View>
								</View>
							</View>
							<View style={{ backgroundColor: 'lightgray', height: 1, width: '100%' }}>
							</View>
							<View style={{ height: '30%', width: '100%', flexDirection: 'row', marginTop: 20 }}>
								<View style={{ height: '100%', width: '33%', alignItems: "center" }}>
									<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Order ID</Text>
									<Text style={{ fontFamily: Fonts.bold, fontSize: 12, color: Constants.black, marginLeft: 14, textTransform: "uppercase" }}>#{modalData.order_id}</Text>
								</View>
								<View style={{ backgroundColor: 'lightgray', height: '70%', width: 1 }}>
								</View>
								<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
									<Text style={{
										fontSize: 12, marginLeft: 14,
										color: Constants.black, fontFamily: Fonts.medium
									}}>Amount </Text>
									<Text style={{
										marginTop: 1, fontFamily: Fonts.bold,
										fontSize: 12, color: Constants.black, marginLeft: 14
									}}>{(modalData.total_amount_after_tax + modalData.delivery_charges) - modalData.discount} {Constants.currency} </Text>
								</View>
								<View style={{ backgroundColor: 'lightgray', height: '70%', width: 1 }}>
								</View>
								<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
									<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Customer </Text>
									<Text style={{ marginTop: 1, fontFamily: Fonts.bold, fontSize: 12, color: Constants.black, textAlign: "center" }}>{modalData.address ? modalData.address.delivery.full_name : "N/A"}</Text>
								</View>
							</View>
							<View style={{ height: '20%', width: '100%', flexDirection: 'row', alignItems: 'center' }}>
								<TouchableOpacity style={{
									height: 30, alignItems: 'center',
									justifyContent: 'center', backgroundColor: Constants.green,
									width: '48%', marginRight: 10,
									borderRadius: 20, marginTop: 20,
									marginLeft: '8%'
								}} onPress={markCompleteFunction}>
									<Text style={{
										fontSize: 13, color: 'white',
										fontFamily: Fonts.bold
									}}>{statusMark}</Text>
								</TouchableOpacity>
								<TouchableOpacity style={{
									height: 30, alignItems: 'center',
									justifyContent: 'center', width: '35%',
									marginRight: 10, borderRadius: 20,
									marginTop: 20, borderWidth: 1,
									borderColor: 'lightgray'
								}} onPress={markIncompleteFunction}>
									<Text style={{ fontSize: 13, color: Constants.black, fontFamily: Fonts.semiBold }}>UNDELIVERED</Text>
								</TouchableOpacity>
							</View>
						</View>
					</View> :
					<View style={styles.container}>
						<View style={{ height: "auto", width: '95%', flexDirection: 'column', backgroundColor: 'white', borderRadius: 10 }}>
							<View style={{ height: 60, width: '100%', flexDirection: 'row', marginBottom: 10 }}>
								<View style={{ height: '100%', width: '80%', flex: 1, flexDirection: 'row' }}>
									<View style={{ marginTop: 18, marginLeft: 15, flexDirection: 'row', }}>
										<Text style={{ color: Constants.black, fontSize: 16, fontFamily: Fonts.semiBold, width: '80%', marginTop: -7 }}>Change Order status</Text>
										<TouchableOpacity style={{ width: '20%', alignItems: 'flex-end', height: "100%", justifyContent: "flex-start", marginTop: -5 }} onPress={closeUndeliverOrderModal}>
											<Image source={Images.ImgClose} style={styles.closeIcon}></Image>
										</TouchableOpacity>
									</View>
								</View>
							</View>
							<View style={{ backgroundColor: 'lightgray', height: 1, width: '100%', marginTop: -30 }}>
							</View>
							<View style={{ height: 40, width: '100%', flexDirection: 'row', marginTop: 20 }}>
								<View style={{ height: '100%', width: '33%', alignItems: "center" }}>
									<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Order ID</Text>
									<Text style={{ fontFamily: Fonts.bold, fontSize: 12, color: Constants.black, marginLeft: 14, textTransform: "uppercase" }}>#{modalData.order_id}</Text>
								</View>
								<View style={{ backgroundColor: 'lightgray', height: '70%', width: 1 }}>
								</View>
								<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
									<Text style={{
										fontSize: 12, marginLeft: 14,
										color: Constants.black,
										fontFamily: Fonts.medium
									}}>Amount </Text>
									<Text style={{
										marginTop: 1, fontFamily: Fonts.bold,
										fontSize: 12, color: Constants.black,
										marginLeft: 14
									}}>{(modalData.total_amount_after_tax + modalData.delivery_charges) - modalData.discount} {Constants.currency} </Text>
								</View>
								<View style={{ backgroundColor: 'lightgray', height: '70%', width: 1 }}>
								</View>
								<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
									<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Customer </Text>
									<Text style={{ marginTop: 1, fontFamily: Fonts.bold, fontSize: 12, color: Constants.black, textAlign: "center" }}>{modalData.address ? modalData.address.delivery.full_name : "N/A"}</Text>
								</View>
							</View>
							<View style={{ height: 60, width: '100%', flexDirection: 'row', marginTop: 5 }}>
								<TouchableOpacity style={{ height: 30, alignItems: 'center', justifyContent: 'center', width: '48%', marginRight: 10, borderRadius: 20, marginTop: 10, marginLeft: '8%', borderWidth: 1, borderColor: 'lightgray', borderRadius: 20, }}
									onPress={markCompleteFunction}>
									<Text style={{ fontSize: 13, fontFamily: Fonts.semiBold, color: Constants.black }}>{statusMark}</Text>
								</TouchableOpacity>
								<TouchableOpacity style={{ height: 30, alignItems: 'center', justifyContent: 'center', width: '35%', marginRight: 10, borderRadius: 20, marginTop: 10, backgroundColor: Constants.green, }}
									onPress={markIncompleteFunction}>
									<Text style={{ fontSize: 13, color: 'white', fontFamily: Fonts.bold }}>UNDELIVERED</Text>
								</TouchableOpacity>
							</View>
							<View style={{ backgroundColor: 'lightgray', width: '100%', height: 1 }}></View>
							<View style={{ height: 'auto', width: '100%', marginTop: 10, }}>
								<TouchableOpacity style={styles.checkBoxContainer} onPress={() => this.setState({ reasonIndex: 1 })}>
									<View style={[styles.checkBox, {
										borderColor: this.state.reasonIndex == 1 ? Constants.green : "lightgrey",
										borderWidth: this.state.reasonIndex == 1 ? 5 : 2,
									}]}></View>
									<Text style={{
										color: Constants.black,
										fontSize: 13, fontFamily: Fonts.medium
									}}>Location is not Correct</Text>
								</TouchableOpacity>
								<TouchableOpacity style={styles.checkBoxContainer} onPress={() => this.setState({ reasonIndex: 2 })}>
									<View style={[styles.checkBox, {
										borderColor: this.state.reasonIndex == 2 ? Constants.green : "lightgrey",
										borderWidth: this.state.reasonIndex == 2 ? 5 : 2,
									}]}></View>
									<Text style={{
										color: Constants.black,
										fontSize: 13, fontFamily: Fonts.medium
									}}>Not able to Contact to Customer</Text>
								</TouchableOpacity>
								<TouchableOpacity style={styles.checkBoxContainer} onPress={() => this.setState({ reasonIndex: 3 })}>
									<View style={[styles.checkBox, {
										borderColor: this.state.reasonIndex == 3 ? Constants.green : "lightgrey",
										borderWidth: this.state.reasonIndex == 3 ? 5 : 2,
									}]}></View>
									<Text style={{
										color: Constants.black,
										fontSize: 13, fontFamily: Fonts.medium
									}}>My reason is not listed</Text>
								</TouchableOpacity>
								{
									this.state.reasonIndex == 3 ?
										<TextInput
											style={styles.placeHolderStyle}
											multiline={true}
											numberOfLines={4}
											placeholder="Enter Your Reason Here ..."
											placeholderTextColor={Constants.placeHolderColor}
											onChangeText={(reason) => this.setState({ reason })}
											value={this.state.reason}
										></TextInput> : null
								}
							</View>
							<TouchableOpacity style={styles.submitButton} onPress={() => submitReasonFunction(this.state)}>
								<Text style={{
									color: "white",
									fontSize: 16,
									fontFamily: Fonts.bold
								}}>SUBMIT</Text>
							</TouchableOpacity>
						</View>
					</View>}
			</Modal>
		);
	}
}

const styles = StyleSheet.create({
	submitButton: {
		backgroundColor: Constants.green,
		margin: 10, marginTop: 0, borderRadius: 25,
		width: 100, height: 40,
		justifyContent: "center", alignItems: "center"
	},
	placeHolderStyle: {
		width: "95%",
		margin: 10,
		borderWidth: 1,
		borderColor: "lightgrey",
		height: 90,
		borderRadius: 10,
		padding: 10
	},
	checkBox: {
		borderRadius: 15,
		width: 20,
		height: 20,
		margin: 15,
		marginTop: 20,
		marginBottom: 20,
	},
	checkBoxContainer: {
		flexDirection: "row",
		height: 40,
		width: "100%",
		alignItems: "center"
	},
	closeIcon: {
		width: 15,
		height: 15,
		marginBottom: 5,
		marginRight: 15
	},
	container: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1,
		backgroundColor: 'rgba(52, 52, 52, 0.8)'
	},
	Header: {
		width: '100%',
		height: 200,
		alignItems: 'center'
	},
	productBox: {
		marginTop: 10,
		backgroundColor: "white",
		height: 180,
		width: '95%',
		borderRadius: 10,
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: .8,
		shadowRadius: 2,
		elevation: 1,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: '2.5%',
		marginBottom: 10
	},
	headerlabel: {
		marginTop: 50, fontSize: 16,
		color: Constants.white, fontFamily: Fonts.bold
	}
});