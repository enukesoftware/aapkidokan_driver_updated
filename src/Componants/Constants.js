import { Dimensions, Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
const productionUrl = "https://aapkidokan.com/";
const yiipro = "http://aapkidokan.yiipro.com/"
const testingUrl = "https://test.aapkidokan.com/";
const baseUrl = productionUrl;

export default Constants = {
	width: Dimensions.get("window").width,
	height: Dimensions.get("window").height,
	statusBarHeight: (Platform.OS == "ios" ? getStatusBarHeight(true) : 0),
	green: "#1faf4b",
	gold: "gold",
	red: "#d90c23",
	placeHolderColor: "#3e3f3f",
	black: "#4d4d4d",
	backgroundPageColor: "#f5f5f5",
	lightgreen: '#D0EFDC',
	currency: "PKR",
	white: "#FFFFFF",
	backgroundScreenColor: 'rgba(245, 245, 245, 1)',
	backgroundPending: 'rgba(237, 222, 172, 1)',
	userToken: '',
	url: baseUrl + "api/driver/",
	imageBaseURL: baseUrl + "uploads/",
	// url: "https://aapkidokan.com/api/driver/",
	// imageBaseURL: "https://aapkidokan.com/uploads/",
	user: {},
	version: "1.11",
	versionIos: "1.10",
	versionAndroid: "1.11.1",
	GOOGLE_API_KEY: "AIzaSyD4fErzG1FxAsRLTzEhaovHH6Rh48TB2kI",
	FCM_TOKEN: "",
	TOKEN: "",
	sharedPreference: { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' },
	LISTEN_NOTIFICATION: "listen:notification",
	WATCH_LOCATION: "watch:location",
	CANCEL_WATCH_LOCATION: "cancel:watch:location",
	is_available: true,
	projectId: "aapkidokan-4c311",
	timerId: null
}