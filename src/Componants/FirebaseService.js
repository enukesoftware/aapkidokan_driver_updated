import firebase from 'react-native-firebase';

export const insertDriver = (data) => {
	// console.log("inserting:---" + JSON.stringify(order));
	return firebase.database()
		.ref('locations/')
		.child(data.driverId)
		.child(data.order_id)
		.child("123")
		.set({
			"orderStatus": data.status.toString(),
			"deliverLatitude": data.deliverLatitude,
			"deliverLongitude": data.deliverLongitude,
			"mLattitude": data.mLattitude,
			"mLongitude": data.mLongitude,
			"orderStatus": data.status,
			"pickUpLatitude": data.pickUpLatitude,
			"pickUpLongitude": data.pickUpLongitude
		});
}

export const deleteOrder = (driver_id, order_id) => {
	return firebase.database().ref('locations/')
		.child(driver_id)
		.child(order_id).remove();
}