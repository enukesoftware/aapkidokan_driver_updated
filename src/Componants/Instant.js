import React, { useState, useEffect, useRef } from 'react';
import { View, Text, Image } from 'react-native';
import OrderStyles from "../Screens/Tabbar/OrderStyle";
import Constants from "./Constants";
import Fonts from "./Fonts";
import Images from "./Images";
import moment from 'moment';
import RenderVerticalBorder from "./RenderVerticalBorder";

function useInterval(callback, delay) {
  const savedCallback = useRef();
  // console.log("calling timer 2");

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    // console.log("calling timer 3");

    function tick() {
      savedCallback.current();
    }
    let id = setInterval(tick, delay);
    return () => clearInterval(id);
  }, [delay]);
}

export default Instant = (props) => {
  // let { order } = props;
  const [order, setOrder] = useState(null);
  const [time, setTime] = useState({ hour: 0, minute: 0, seconds: 0 });
  const [isLate, setLate] = useState(false);
  const [timeText, setTimeText] = useState("Hour");

  useInterval(() => {
    setTime(moment(props.order.deliver_end_time).diff((new Date()), "m"));
    // console.log("calling timer 1");
    let late = moment(props.order.deliver_end_time).diff((new Date()), "m");
    let late1 = moment(props.order.deliver_end_time).diff((new Date()), "s");
    // late = -55;
    if (late && late < 0) {
      late = -1 * late;
      setLate(true);
    }
    let time1 = { hour: 0, minute: 0, seconds: late1 % 60 };
    time1 = { hour: 0, minute: 0, seconds: late1 % 60 };
    if (time1.seconds < 1) {
      time1.seconds = time1.seconds * -1;
    }
    if ((late % 60) == 0) {
      time1.hour = late / 60;
    } else if (late < 60) {
      time1.minute = late;
    } else {
      time1.hour = ~~(late / 60);
      time1.minute = late - (time1.hour * 60)
    }
    console.log
    setTime(time1);
    setOrder(props.order);
  }, 1000);

  return (
    order ?
      <View style={OrderStyles.expressBox}>
        <View style={OrderStyles.expressLabelView}>
          <Text style={{ color: Constants.black, fontFamily: Fonts.regular }}>Instant Delivery</Text>
        </View>
        <RenderVerticalBorder />
        <Image style={OrderStyles.lateImage} resizeMode="contain" source={Images.ImgDirection} />
        <View style={isLate ? OrderStyles.lateView : OrderStyles.NotLateView}>
          <Text style={isLate ? OrderStyles.latetext : OrderStyles.NotLateViewText} numberOfLines={1}>
            {(time.hour && time.minute) ?
              (time.hour + '.' + time.minute) : (time.hour ? time.hour : time.minute)}.{time.seconds}
          </Text>
          {
            < View style={{ marginLeft: 10 }}>
              <Text style={OrderStyles.lateText1}>{(time.hour || (time.hour && time.minute)) ? "Hour" : ((!time.hour && !time.minute) ? "Second's" : "Min's")}</Text>
              <Text style={OrderStyles.lateText1}>{isLate ? "Late" : "Left"}</Text>
            </View>
          }
        </View>
      </View> : <View />
  );
}