import React from 'react';
import { View } from 'react-native';

export default RenderVerticalBorder = () => {
  return (
    <View style={{ backgroundColor: 'white', height: '100%', width: 1 }}>
      <View style={{ backgroundColor: 'white', height: '25%', width: 1 }}></View>
      <View style={{ backgroundColor: 'gray', height: '50%', width: 1 }}></View>
      <View style={{ backgroundColor: 'white', height: '25%', width: 1 }}></View>
    </View>
  )
}