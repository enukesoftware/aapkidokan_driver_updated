import React, { Component } from 'react';
import { Linking, View, DeviceEventEmitter, Platform, PermissionsAndroid, Alert } from 'react-native';
import AndroidOpenSettings from 'react-native-android-open-settings';
import Constants from './Constants';
import { connect } from 'react-redux';
import * as Actions from '../Redux/Action/Index';
import * as FirebaseService from './FirebaseService';
import Geolocation from '@react-native-community/geolocation';

// const Geolocation = Platform.OS == "android" ? require("react-native-geolocation-service") : require("@react-native-community/geolocation")

class WatchLocation extends Component {
	state = {};

	componentDidMount = () => {
		DeviceEventEmitter.addListener(Constants.WATCH_LOCATION, this.startWatchLocation);
		DeviceEventEmitter.addListener(Constants.CANCEL_WATCH_LOCATION, this.clearWatchLocation);
		console.log(this.props);
	}

	componentWillUnmount = () => {
		DeviceEventEmitter.removeAllListeners();
	}

	startWatchLocation = () => {
		if (this.props.getDriver._id) {
			if (Platform.OS === 'android') {
				this.requestPermission();
			} else this.myLocation();
		}
	}

	getIosLocation = () => {
		Geolocation.getCurrentPosition((position) => {
			console.log("first location fetched:---" + JSON.stringify(position));
			// alert("first location fetched:---" + JSON.stringify(position));
			this.props.saveLocation(position.coords);
			this.updateLocationInOrders();
		}, (error) => {
			// See error code charts below.
			console.log(error.code, error.message);
			this.requestAlert();
			// alert(error.code, error.message);
		});
		this.watchID = Geolocation.watchPosition((position) => {
			this.props.saveLocation(position.coords);
			this.updateLocationInOrders();
			console.log("watch location fetched:---" + JSON.stringify(this.props.getLocation));
			// alert("watch location fetched:---" + JSON.stringify(this.props.getLocation));
		},
			(error) => {
				// See error code charts below.
				console.log(error.code, error.message);
				// alert(error.code, error.message);
				this.requestAlert();
				// this.props.saveLocation(position);
			},
			{
				enableHighAccuracy: true,
				fastestInterval: 8000,
				distanceFilter: 2,
				showLocationDialog: true,
				forceRequestLocation: true
			}
		);
	}

	// getLocation = () => {
	// 	Geolocation.default.getCurrentPosition((position) => {
	// 		console.log("first location fetched:---" + JSON.stringify(position));
	// 		// alert("first location fetched:---" + JSON.stringify(position));
	// 		this.props.saveLocation(position.coords);
	// 		this.updateLocationInOrders();
	// 	}, (error) => {
	// 		// See error code charts below.
	// 		// alert(error.code, error.message);
	// 		console.log(error.code, error.message);
	// 	});

	// 	this.watchID = Geolocation.default.watchPosition(
	// 		(position) => {
	// 			console.log("watch location fetched:---" + JSON.stringify(position));
	// 			// alert("watch location fetched:---" + JSON.stringify(position));
	// 			this.props.saveLocation(position.coords);
	// 			this.updateLocationInOrders();
	// 		},
	// 		(error) => {
	// 			// See error code charts below.
	// 			console.log(error.code, error.message);
	// 			// alert(error.code, error.message);
	// 		},
	// 		{
	// 			enableHighAccuracy: true,
	// 			fastestInterval: 8000,
	// 			distanceFilter: 2,
	// 			showLocationDialog: true,
	// 			forceRequestLocation: true
	// 		}
	// 	);
	// }

	myLocation = () => {
		if (Platform.OS === 'android') {
			this.getIosLocation();
		} else {
			this.getIosLocation();
		}
	}

	updateLocationInOrders = async () => {
		let orders = this.props.getDeliveryOrders;
		for (let i = 0; i < orders.length; i++) {
			let data = {};
			data.pickUpLatitude = orders[i].address.pickup.coordinates.latitude;
			data.pickUpLongitude = orders[i].address.pickup.coordinates.longitude;
			data.deliverLatitude = orders[i].address.delivery.coordinates.latitude;
			data.deliverLongitude = orders[i].address.delivery.coordinates.longitude;
			data.mLattitude = this.props.getLocation.latitude;
			data.mLongitude = this.props.getLocation.longitude;
			data.driverId = this.props.getDriver._id;
			data.order_id = orders[i].order_id;
			data.status = orders[i].status;
			console.log("Updating order index:--" + i + " and order id:--" + data.order_id);
			// alert("Updating order index:--" + i + " and order id:--" + data.order_id);
			await FirebaseService.insertDriver(data);
		}
	}

	requestPermission = async () => {
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
				{
					'title': 'Location Permission',
					'message': 'App needs access to your location '
				}
			)
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				// Toast.showWithGravity("Location Permission Granted.", Toast.SHORT, Toast.BOTTOM);
				this.myLocation();
			} else {
				this.requestAlert(granted);
				// Toast.showWithGravity("Location Permission Not Granted", Toast.SHORT, Toast.BOTTOM);
			}
		} catch (err) {
			console.warn(err)
			Toast.showWithGravity("Location Permission Error", Toast.SHORT, Toast.BOTTOM);
		}
	}

	requestAlert = (granted) => {
		let messsage = "Please change the location permission as 'Always', we need rider location to track the grocery orders.";
		const isIOS = Platform.OS == "ios";
		// if (granted) {
		// 	messsage = "Your location permission is as '" + granted + "', but we need the location services for tracking the order";
		// }
		Alert.alert('', messsage, [
			{
				text: 'CANCEL',
				onPress: () => console.log('Cancel Pressed'),
				style: 'cancel',
			},
			{
				text: 'ENABLE', onPress: () => {
					if (isIOS) {
						// this.requestPermissionIos();
						Linking.canOpenURL('app-settings:').then(supported => {
							if (!supported) {
								console.log('Can\'t handle settings url');
							} else {
								Linking.openURL('app-settings:');
							}
						}).catch(err => console.error('An error occurred', err));
					} else {
						// AndroidOpenSettings.locationSourceSettings();
						AndroidOpenSettings.appDetailsSettings();
					}
				}
			},
		],
			{ cancelable: false }
		)
	}

	clearWatchLocation = () => {
		if (this.watchID) {
			console.log("WATCH_LOC_UNMOUNT:" + this.watchID)
			Geolocation.clearWatch(this.watchID);
		} else {
			console.log("WATCH_LOC_UNMOUNT")
		}
	}

	componentWillUnmount() {
		this.clearWatchLocation();
	}

	render() {
		return (
			<View></View>
			// <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
			// 	<Text>Hello</Text>
			// </View>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		getDriver: state.getDriver.user,
		getToken: state.getToken.token,
		getDeliveryOrders: state.getDeliveryOrders.orders,
		getLocation: state.getLocation.location,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		saveDeliveryOrders: (data) => dispatch(Actions.saveOrderToDelivery(data)),
		saveLocation: (data) => dispatch(Actions.saveLocation(data)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(WatchLocation)