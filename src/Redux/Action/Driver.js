import * as Types from '../Types';

export const saveDriverData = driverdata => {
	return {
		type: Types.SAVE_DRIVER_DATA,
		payload: driverdata
	};
}

export const saveToken = token => {
	console.log("saving token:--" + token);
	return {
		type: Types.SAVE_TOKEN,
		payload: token
	};
}

export const saveLocation = data => {
	console.log("saving location:--" + data);
	return {
		type: Types.SAVE_LOCATION,
		payload: data
	};
}

export const saveFcmToken = token => {
	return {
		type: Types.SAVE_FCM_TOKEN,
		payload: token
	};
}

export const saveOrderToDelivery = orders => {
	return {
		type: Types.ORDER_FOR_DELIVERY,
		payload: orders
	};
}

export const saveNotification = data => {
	return {
		type: Types.SAVE_NOTIFICATION,
		payload: data
	};
}

export const deleteAllNotifications = () => {
	return {
		type: Types.DELETE_ALL_NOTIFICATIONS,
		payload: null
	};
}