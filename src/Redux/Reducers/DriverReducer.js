import * as Types from '../Types'

const initialState = {
	user: {},
	location: {},
	token: '',
	fcmToken: '',
	orders: [],
	notifications: [],
};

export const DriverReducer = (state = initialState, action) => {
	switch (action.type) {
		case Types.SAVE_DRIVER_DATA:
			// console.log("setting driver data:==" + JSON.stringify(action))
			return {
				...state,
				user: action.payload
			};
		default:
			return state;
	}
};

export const LocationReducer = (state = initialState, action) => {
	switch (action.type) {
		case Types.SAVE_LOCATION:
			console.log("setting location:==" + JSON.stringify(action))
			return {
				...state,
				location: action.payload
			};
		default:
			return state;
	}
};

export const FcmTokenReducer = (state = initialState, action) => {
	console.log("saving fcm token:--" + action);

	switch (action.type) {
		case Types.SAVE_FCM_TOKEN:
			return {
				...state,
				fcmToken: action.payload
			};
		default:
			return state;
	}
};

export const TokenReducer = (state = initialState, action) => {
	switch (action.type) {
		case Types.SAVE_TOKEN:
			return {
				...state,
				token: action.payload
			};
		default:
			return state;
	}
};

export const SaveDeliveryOrder = (state = initialState, action) => {
	switch (action.type) {
		case Types.ORDER_FOR_DELIVERY:
			return {
				...state,
				orders: action.payload
			};
		default:
			return state;
	}
};

export const SaveNotification = (state = initialState, action) => {
	switch (action.type) {
		case Types.SAVE_NOTIFICATION:
			return {
				...state,
				notifications: [...state.notifications, ...action.payload]
			};
		default:
			return state;
	}
};


export const DeleteNotifications = (state = initialState, action) => {
	switch (action.type) {
		case Types.DELETE_ALL_NOTIFICATIONS:
			return {
				...state,
				notifications: []
			};
		default:
			return state;
	}
};