import { createStore, combineReducers } from "redux";
import { DriverReducer, LocationReducer, TokenReducer, FcmTokenReducer, SaveDeliveryOrder,SaveNotification,DeleteNotifications } from "../Reducers/DriverReducer";


const rootReducer = combineReducers({
	getDriver: DriverReducer,
	getToken: TokenReducer,
	getFcmToken: FcmTokenReducer,
	getDeliveryOrders: SaveDeliveryOrder,
	getLocation: LocationReducer,
	getNotifications:SaveNotification,
	deleteNotifications:DeleteNotifications
})

export const configureStore = () => {
	return createStore(rootReducer,/* preloadedState, */
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	)
}