export const SAVE_DRIVER_DATA = "SAVEDRIVERTDATA";
export const SAVE_TOKEN = "SAVE_TOKEN";
export const SAVE_LOCATION = "SAVE_LOCATION";
export const SAVE_FCM_TOKEN = "SAVE_FCM_TOKEN";
export const ORDER_FOR_DELIVERY = "ORDER_FOR_DELIVERY";
export const SAVE_NOTIFICATION = "SAVE_NOTIFICATION";
export const DELETE_ALL_NOTIFICATIONS = "DELETE_ALL_NOTIFICATIONS";
