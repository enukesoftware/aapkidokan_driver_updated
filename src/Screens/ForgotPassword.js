
import React, { Component } from 'react';
import { Image, View, Dimensions, StyleSheet, Text, TextInput, TouchableOpacity, ImageBackground, Alert } from 'react-native'
import constants from '../Componants/Constants';
import Images from '../Componants/Images';
import Fonts from '../Componants/Fonts';
import * as ApiManager from './../ApiManager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import NavigationService from '../Componants/NavigationService';

export default class ForgotPasswordScreen extends Component {
	static navigationOptions = ({ navigation }) => ({
		header: null
	});

	state = {
		contact_number: '',
		loading: false
	}

	sendOtp = () => {
		if (!this.state.contact_number || !this.state.contact_number.length) {
			Toast.showWithGravity("Please enter your Mobile Number", Toast.LONG, Toast.BOTTOM);
		} else if (isNaN(this.state.contact_number) || (!isNaN(this.state.contact_number) && ((this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 11)) || (!this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 10))))) {
			Toast.showWithGravity("Mobile Number is not valid", Toast.LONG, Toast.BOTTOM);
		} else {
			this.setState({ loading: true });
			ApiManager.forgotPassword({
				contact_number: this.state.contact_number
			}).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => console.log("loading paused"))
				}, 100);
				setTimeout(() => {
					if (res && res.success && res.code == 200) {
						let body = {
							verification_token: res.data.verification_token,
							contact_number: this.state.contact_number,
							reset: true
						}
						NavigationService.navigate("GenerateOtp", { data: body });
					} else {
						Toast.showWithGravity(res.singleStringMessage, Toast.LONG, Toast.BOTTOM);
					}
				}, 500);
			}).catch((err) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => console.log("loading paused"))
				}, 100);
				setTimeout(() => {
					Toast.showWithGravity(err.singleStringMessage, Toast.LONG, Toast.BOTTOM);
				}, 500);
				console.log("Otp error" + JSON.stringify(err));
			})
		}
	}

	render() {
		console.log(this.props.navigation.state.routeName);
		return (
			<ImageBackground source={Images.ImgBackground} style={{ flex: 1 }}>
				<View style={styles.outerSection}>
					<Spinner
						size="large"
						visible={this.state.loading}
						color={constants.green}
						textContent={'Please wait . . .'}
						textStyle={{ color: "white", fontFamily: Fonts.bold }}
					/>
					<View style={[styles.innerSection, { minHeight: 250, paddingBottom: 30 }]}>
						<View style={styles.logoSection}>
							<Image source={Images.ImgLogo} style={styles.logo}></Image>
						</View>
						<Text style={[styles.innerlabel, { fontSize: 15, marginBottom: 10, marginTop: 20 }]}>Enter Mobile Number to Send OTP</Text>
						<View style={[styles.inputSection, { marginBottom: 15 }]}>
							<Image source={Images.ImgMobile} style={styles.inputLogo} />
							<TextInput
								style={styles.input}
								value={this.state.contact_number}
								maxLength={11}
								keyboardType="phone-pad"
								placeholder='Enter Mobile Number'
								placeholderTextColor={constants.placeHolderColor}
								onChangeText={(text) => { this.setState({ contact_number: text }) }}
								returnKeyType="done"
								onSubmitEditing={() => this.sendOtp()}
							/>
						</View>
						<TouchableOpacity style={styles.loginButton}
							onPress={() => this.sendOtp()}>
							<Text style={[styles.loginText, { color: "white" }]}>SEND OTP</Text>
						</TouchableOpacity>
					</View>
				</View>
			</ImageBackground>
		)
	}
}

const styles = StyleSheet.create({
	bgImage: {
		width: "100%",
		height: "100%",
		position: "absolute"
	},
	outerSection: {
		flex: 1,
		// height: (Dimensions.get("window").height),
		alignItems: 'center',
		justifyContent: 'center',
		position: "relative"
	},
	innerSection: {
		backgroundColor: 'white',
		alignItems: 'center',
		shadowOpacity: 1,
		shadowRadius: 3,
		shadowOffset: { height: 0, width: 0 },
		elevation: 1, zIndex: 0, borderRadius: 10,
		width: ((Dimensions.get("window").width) - 50),
		minHeight: 480,
		position: "relative",
		marginTop: 45,
		paddingTop: 50,
		marginBottom: 0
	},
	innerImage: {
		width: 100, height: 100,
		borderRadius: 50,
		zIndex: 1, marginTop: -510
	},
	innerlabel: {
		marginTop: 10,
		fontSize: 18,
		color: constants.black,
		color: "black", fontFamily: Fonts.bold
	},
	logoSection: {
		width: 120,
		height: 115,
		position: "absolute",
		zIndex: 2,
		top: 0,
		backgroundColor: constants.green,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 65,
		top: -60
	},
	logo: { width: 80, height: 50 },
	inputLogo: {
		width: 20,
		height: 20,
		resizeMode: 'contain',
		marginTop: 14,
		marginLeft: 15
	},
	forgotLine: {
		width: "auto", height: 30,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121',
		margin: 5
	},
	loginText: {
		fontSize: 22,
		color: constants.black,
		color: "black", fontFamily: Fonts.bold,
	},
	input: {
		marginLeft: 10,
		width: "80%",
	},
	inputSection: {
		borderWidth: 1,
		borderColor: 'lightgrey',
		borderRadius: 25,
		width: '85%',
		height: 50,
		flexDirection: "row",
		marginTop: 10,
		marginBottom: 5
	},
	loginButton: {
		borderWidth: 1,
		minWidth: '85%',
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5,
		borderRadius: 25,
		backgroundColor: constants.green,
		borderColor: constants.green
	},
	socialSection: {
		width: "auto",
		height: 70,
		alignContent: "center",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	socialButton: {
		height: 55,
		width: 55,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 2,
		borderRadius: 50,
		marginRight: 5
	},
	signUpBar: {
		width: "80%",
		height: 40,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	signUpLline: {
		height: 1,
		backgroundColor: "lightgrey",
		flex: 1
	}
});