import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TextInput, TouchableOpacity, ImageBackground } from 'react-native';
import Images from '../Componants/Images';
import Constants from '../Componants/Constants';
import Fonts from '../Componants/Fonts';
import * as ApiManager from './../ApiManager/ApiManager';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import SInfo from 'react-native-sensitive-info';
import NavigationService from '../Componants/NavigationService';

export default class GenerateOtpScreen extends Component {
	static navigationOptions = {
		// title: 'Please sign in',
		header: null //hide the header
	};

	constructor(props) {
		super(props)

		this.state = {
			otp: '',
			verification_token: '',
			contact_number: '',
			loading: false,
			resetPassword: false
		}
	}

	componentDidMount = () => {
		let params = this.props.navigation.state.params;
		if (params) {
			this.setState({
				loading: false,
				verification_token: params.data.verification_token,
				contact_number: params.data.contact_number,
				resetPassword: params.data.reset
			}, () => {
				console.log("state is :---" + JSON.stringify(this.state));
			});
		}
	}

	verifyOtp = () => {
		if (!this.state.otp || !this.state.otp.length) {
			Toast.showWithGravity("please enter your OTP", Toast.SHORT, Toast.BOTTOM);
		} else {
			this.setState({ loading: true });
			ApiManager.verifyOtp(this.state).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => console.log("loading paused"))
				}, 100);
				setTimeout(async () => {
					if (res && res.success && res.code == 200) {
						await SInfo.setItem('token', res.data.token + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
						console.log("Otp succes" + JSON.stringify(res));
						Constants.userToken = res.data.token;
						NavigationService.navigate("ResetPassword");
					} else {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
					}
				}, 500);
			}).catch((err) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => console.log("loading paused"))
				}, 100);
				setTimeout(() => {
					Toast.showWithGravity(err.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}, 500);
				console.log("Otp error" + JSON.stringify(err));
			})
		}
	}

	resendOtp = () => {
		this.setState({ loading: true });
		ApiManager.forgotPassword({ contact_number: this.state.contact_number }).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"))
			}, 100);
			setTimeout(() => {
				if (res.success && res.code == 200) {
					Toast.showWithGravity(res.data.message, Toast.LONG, Toast.BOTTOM);
					this.setState({ verification_token: verification_token }, () => {
						console.log(JSON.stringify(this.state));
					})
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.LONG, Toast.BOTTOM);
				}
			}, 500);
		}).catch((err) => {
			this.setState({ loading: false });
			console.log(JSON.stringify(err));
		})
	}

	render() {
		console.log("GenerateOtpScreen");

		return (
			<ImageBackground source={Images.ImgBackground} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<Spinner
					size="large"
					visible={this.state.loading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<View style={[styles.innerSection]}>
					<View style={styles.logoSection}>
						<Image source={Images.ImgLogo} style={styles.logo}></Image>
					</View>
					<Text style={[styles.textlabel]}>Enter OTP to {this.state.resetPassword ? "Reset Your Password" : "Verify Your Account"}</Text>
					<View style={[styles.inputSection]}>
						<Image source={Images.ImgMobile} style={styles.inputLogo} />
						<TextInput
							style={styles.input}
							placeholder={"Enter OTP"}
							placeholderTextColor={Constants.placeHolderColor}
							value={this.state.otp}
							keyboardType={'number-pad'}
							onChangeText={(value) => this.setState({ otp: value })}
							maxLength={10}
							returnKeyType="done"
							onSubmitEditing={() => this.verifyOtp()}
						/>
					</View>
					<TouchableOpacity style={styles.loginButton}
						onPress={() => this.verifyOtp()}>
						<Text style={[styles.loginText]}>VERIFY OTP</Text>
					</TouchableOpacity>
				</View>
			</ImageBackground>
		);
	}

}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#2c3e50',
		justifyContent: 'center'
	},
	loginContainer: {
		width: '100%',
		height: '100%',

	},
	logo: {
		// position: 'absolute',
		width: '100%',
		height: '100%'
	},
	formContainer: {
		padding: 20
	},
	input: {
		marginLeft: 10,
		width: "80%",
	},
	// input: {
	// 	height: 40,
	// 	backgroundColor: 'rgba(225,225,225,0.2)',
	// 	marginBottom: 10,
	// 	padding: 10,
	// 	paddingBottom: 0,
	// 	paddingLeft: 0,
	// 	marginLeft: 5,
	// 	color: 'black'
	// },
	buttonContainer: {
		backgroundColor: '#2980b6',
		paddingVertical: 15,
		marginBottom: 5
	},
	innerSection: {
		backgroundColor: 'white',
		alignItems: 'center',
		shadowOpacity: 1,
		shadowRadius: 3,
		shadowOffset: { height: 0, width: 0 },
		elevation: 1, zIndex: 0, borderRadius: 10,
		width: '95%',
		minHeight: 280,
	},
	logoSection: {
		width: 120,
		height: 115,
		position: "absolute",
		zIndex: 2,
		backgroundColor: Constants.green,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 65,
		top: -60
	},
	logo: { width: 80, height: 50 },
	textlabel: {
		marginTop: 70, fontSize: 18,
		color: Constants.black, fontFamily: Fonts.bold
	},
	inputSection: {
		borderWidth: 1,
		borderColor: 'lightgrey',
		borderRadius: 25,
		width: '85%',
		height: 50,
		flexDirection: "row",
		marginTop: 25,
		marginBottom: 5
	},
	inputLogo: {
		width: 20,
		height: 20,
		resizeMode: 'contain',
		marginTop: 14,
		marginLeft: 15,
		marginRight: 15

	},
	loginButton: {
		borderWidth: 1,
		minWidth: '85%',
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 20,
		borderRadius: 25,
		backgroundColor: Constants.green,
		borderColor: Constants.green
	},
	loginText: {
		fontSize: 20,
		color: Constants.white,
		fontFamily: Fonts.bold,
	},

	buttonText: {
		color: '#fff',
		textAlign: 'center',
		fontFamily: Fonts.bold
	}
});