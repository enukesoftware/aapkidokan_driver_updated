import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import Constants from '../Componants/Constants';
import CardView from 'react-native-cardview';
import moment from 'moment';
import Fonts from '../Componants/Fonts';

export default class OrderDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			order: {},
			products: [],
		};
	}

	static navigationOptions = {
		title: 'View Detail',
		headerStyle: {
			backgroundColor: Constants.green,
			alignItems: "flex-start",
			textAlign: "left"
		},
		headerTintColor: '#fff',
		headerTitleStyle: {
			fontFamily: Fonts.bold,
			fontSize: 18,
			position: "absolute",
			left: -10
			// marginBottom: 10
		},
	};

	componentWillMount() {
		if (this.props.navigation.state.params) {
			console.log(this.props.navigation.state.params.order);
			let paramsData = this.props.navigation.state.params.order;
			this.setState({
				products: paramsData.products,
				order: paramsData
			})
		}
	}

	render() {
		return (
			<View style={{
				flex: 1,
				justifyContent: "flex-start",
			}}>
				<ScrollView style={{ flex: 1 }}>
					<CardView cardElevation={5}
						cardMaxElevation={5}
						cornerRadius={5}
						style={styles.productBox}>
						<View style={{
							height: 90, width: "100%",
							backgroundColor: "white",
							flexDirection: "row",
							justifyContent: "flex-start",
							alignItems: "center"
						}}>
							<View style={{
								width: "20%",
								height: "100%", alignItems: "center",
								justifyContent: "center",
							}}>
								<Image source={require("../assets/imgs/tea.jpg")}
									style={{ height: 82, width: "90%", resizeMode: "contain" }}></Image>
							</View>
							<View style={{
								width: "65%", alignItems: "flex-start",
								justifyContent: "center",
								height: 90, paddingLeft: 5
							}}>
								<Text numberOfLines={2} style={{
									fontSize: 12,
									fontFamily: Fonts.medium, color: Constants.black
								}}>React Native JS code runs as a web worker inside this tab.,React Native JS code runs as a web worker inside this tab.</Text>
								<Text style={{
									color: Constants.black,
									fontSize: 13, fontFamily: Fonts.medium,
									textAlign: "left", marginBottom: 3, marginTop: 3
								}}>5 Kg, 2 Unit's</Text>
								<Text style={{
									color: Constants.black,
									fontSize: 13, fontFamily: Fonts.semiBold,
									textAlign: "left"
								}}>{Constants.currency} 99 * 2</Text>
							</View>
							<View style={{
								width: "15%", alignItems: "flex-end",
								height: 90, padding: 5, paddingTop: 15
							}}>
								<Text style={{
									color: Constants.green,
									fontSize: 13, fontFamily: Fonts.semiBold,
									textAlign: "right"
								}}>198 {Constants.currency}</Text>
							</View>
						</View>
					</ CardView>
				</ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	bars: {
		height: 40,
		width: "100%",
		justifyContent: "center",
		paddingLeft: 15, marginTop: 10
	},
	Header: {
		width: '100%',
		height: 200,
		alignItems: 'center'
	},
	productBox: {
		marginTop: 10,
		backgroundColor: "white",
		height: "auto",
		width: '95%',
		borderRadius: 10,
		borderBottomWidth: 3,
		borderColor: Constants.backgroundPageColor,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: '2.5%',
		marginBottom: 10
	},
	headerlabel: {
		marginTop: 50, fontSize: 16,
		color: Constants.white, fontFamily: Fonts.bold
	}
});