
import React, { Component } from 'react';
import { Image, View, Text, TextInput, Dimensions, TouchableOpacity, ImageBackground, StyleSheet } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import constants from '../Componants/Constants';
import Fonts from '../Componants/Fonts';
import * as ApiManager from './../ApiManager/ApiManager';
import Images from '../Componants/Images';
import NavigationService from '../Componants/NavigationService';

export default class ResetPasswordScreen extends Component {
	static navigationOptions = ({ navigation }) => ({
		header: null
	});

	state = {
		password: "",
		re_password: "",
		loading: false
	}

	resetPassword = () => {
		if (!this.state.password || !this.state.password.length) {
			Toast.showWithGravity(
				"Please enter your New Password",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if ((!this.state.re_password || !this.state.re_password.length)) {
			Toast.showWithGravity(
				"Please confirm your New Password",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if (this.state.password != this.state.re_password) {
			Toast.showWithGravity(
				"Password Mismatch",
				Toast.LONG,
				Toast.CENTER //TOP,BOTTOM
			);
		} else {
			this.setState({ loading: true });
			ApiManager.resetPassword({ password: this.state.password }).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => console.log("loading paused"))
				}, 100);
				setTimeout(() => {
					if (res && res.success && res.code == 200) {
						Toast.showWithGravity(res.data.message, Toast.LONG, Toast.BOTTOM);
						NavigationService.navigate("Main");
					} else {
						Toast.showWithGravity(res.singleStringMessage, Toast.LONG, Toast.CENTER);
					}
				}, 500);
			}).catch((err) => {
				this.setState({ loading: false });
				console.log(JSON.stringify(err));
			})
		}
	}

	render() {
		console.log(this.props.navigation.state.routeName);
		return (
			<ImageBackground source={Images.ImgBackground} style={{ flex: 1 }}>
				<View style={styles.outerSection}>
					<Spinner
						size="large"
						visible={this.state.loading}
						color={constants.green}
						textContent={'Please wait . . .'}
						textStyle={{ color: "white", fontFamily: Fonts.bold }}
					/>
					<View style={[styles.innerSection, { minHeight: 250, paddingBottom: 30 }]}>
						<View style={styles.logoSection}>
							<Image source={Images.ImgLogo} style={styles.logo}></Image>
						</View>
						<Text style={[styles.innerlabel, { fontSize: 15, marginBottom: 15, marginTop: 20 }]}>Reset Your Password</Text>
						<View style={styles.inputSection}>
							<Image source={Images.ImgPassword} style={styles.inputLogo} />
							<TextInput
								style={styles.input}
								value={this.state.password}
								placeholder='Enter New Password'
								placeholderTextColor={constants.placeHolderColor}
								autoCapitalize="none"
								secureTextEntry={true}
								onChangeText={(text) => { this.setState({ password: text }) }}
								returnKeyType="next"
							/>
						</View>
						<View style={[styles.inputSection, { marginTop: 15 }]}>
							<Image source={Images.ImgPassword} style={styles.inputLogo} />
							<TextInput
								style={styles.input}
								value={this.state.re_password}
								placeholder='Confirm New Password'
								placeholderTextColor={constants.placeHolderColor}
								autoCapitalize="none"
								secureTextEntry={true}
								onChangeText={(text) => { this.setState({ re_password: text }) }}
								returnKeyType="done"
								onSubmitEditing={() => this.resetPassword()}
							/>
						</View>
						<TouchableOpacity style={[styles.loginButton, { marginTop: 15 }]}
							onPress={() => this.resetPassword()}>
							<Text style={[styles.loginText, { color: "white" }]}>RESET PASSWORD</Text>
						</TouchableOpacity>
					</View>
				</View>
			</ImageBackground>
		)
	}
}

const styles = StyleSheet.create({
	bgImage: {
		width: "100%",
		height: "100%",
		position: "absolute"
	},
	outerSection: {
		flex: 1,
		// height: (Dimensions.get("window").height),
		alignItems: 'center',
		justifyContent: 'center',
		position: "relative"
	},
	innerSection: {
		backgroundColor: 'white',
		alignItems: 'center',
		shadowOpacity: 1,
		shadowRadius: 3,
		shadowOffset: { height: 0, width: 0 },
		elevation: 1, zIndex: 0, borderRadius: 10,
		width: ((Dimensions.get("window").width) - 50),
		minHeight: 480,
		position: "relative",
		marginTop: 45,
		paddingTop: 50,
		marginBottom: 0
	},
	innerImage: {
		width: 100, height: 100,
		borderRadius: 50,
		zIndex: 1, marginTop: -510
	},
	innerlabel: {
		marginTop: 10,
		fontSize: 18,
		color: constants.black,
		color: "black", fontFamily: Fonts.bold
	},
	logoSection: {
		width: 120,
		height: 115,
		position: "absolute",
		zIndex: 2,
		top: 0,
		backgroundColor: constants.green,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 65,
		top: -60
	},
	logo: { width: 80, height: 50 },
	inputLogo: {
		width: 20,
		height: 20,
		resizeMode: 'contain',
		marginTop: 14,
		marginLeft: 15
	},
	forgotLine: {
		width: "auto", height: 30,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121',
		margin: 5
	},
	loginText: {
		fontSize: 22,
		color: constants.black,
		color: "black", fontFamily: Fonts.bold,
	},
	input: {
		marginLeft: 10,
		width: "80%",
	},
	inputSection: {
		borderWidth: 1,
		borderColor: 'lightgrey',
		borderRadius: 25,
		width: '85%',
		height: 50,
		flexDirection: "row",
		marginTop: 10,
		marginBottom: 5
	},
	loginButton: {
		borderWidth: 1,
		minWidth: '85%',
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5,
		borderRadius: 25,
		backgroundColor: constants.green,
		borderColor: constants.green
	},
	socialSection: {
		width: "auto",
		height: 70,
		alignContent: "center",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	socialButton: {
		height: 55,
		width: 55,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 2,
		borderRadius: 50,
		marginRight: 5
	},
	signUpBar: {
		width: "80%",
		height: 40,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	signUpLline: {
		height: 1,
		backgroundColor: "lightgrey",
		flex: 1
	}
});