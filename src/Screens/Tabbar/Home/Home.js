import React, { Component } from 'react';
import {
	View, TouchableOpacity, ActivityIndicator, Linking, FlatList, DeviceEventEmitter, Alert,
	Platform, PermissionsAndroid, Text, Dimensions, StyleSheet, Image, ImageBackground, StatusBar
} from 'react-native';
import Images from '../../../Componants/Images';
import Constants from '../../../Componants/Constants';
import ChangeOrderStatusPopup from '../../../Componants/ChangeOrderStatusPopup';
import * as ApiManager from './../../../ApiManager/ApiManager';
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import call from 'react-native-phone-call';
import Fonts from '../../../Componants/Fonts';
import Toast from 'react-native-simple-toast';
import LaunchNavigator from 'react-native-launch-navigator';
import NavigationService from '../../../Componants/NavigationService';
import * as FirebaseService from '../../../Componants/FirebaseService';
import { connect } from 'react-redux';
import * as Actions from '../../../Redux/Action/Index';
import OrderStyles from '../OrderStyle';
import AndroidOpenSettings from 'react-native-android-open-settings';
import Instant from '../../../Componants/Instant';
import RenderVerticalBorder from '../../../Componants/RenderVerticalBorder';
import Geolocation from '@react-native-community/geolocation';

// const Geolocation = Platform.OS == "android" ? require("react-native-geolocation-service") : require("@react-native-community/geolocation")


class HomeScreen extends Component {

	static navigationOptions = ({ navigation }) => ({
		header: null
	});

	constructor(props) {
		super(props);
		this.state = {
			orderPlaceDate: 'Tue 31 Jan',
			orderScheduleDate: 'Tue 6 Feb',
			modalVisible: false,
			renderStatus: 1,
			totalOrders: [],
			undeliveredOrders: [],
			completedOrders: [],
			totalOrder: 0,
			undeliveredOrder: 0,
			completedOrder: 0,
			markcomplete: true,
			reason: {},
			screenWidth: Dimensions.get("window").width,
			loading: false,
			modalData: {},
			modalLoading: false,
			deliveryOrders: this.props.getDeliveryOrders,
			pageNo: 1,
			isLoading: false,
			isRefreshing: false,
			isListEnd: false,
			loadingMore: false,
			orders: []
		};
		console.log(this.props);

		this.cancelNotificationHandler = this.cancelNotificationHandler.bind(this);
	}

	componentWillMount = () => {
		// this.insertDriverintoFirebase();
	}

	componentDidMount = () => {
		// DeviceEventEmitter.emit(Constants.WATCH_LOCATION);
		// this.checkPermission();

	}

	componentWillUnmount = () => {
		DeviceEventEmitter.removeAllListeners();
	}

	cancelNotificationHandler() {
		console.log("EVENT")
		this.refreshOrder()
	}

	setModalVisible(visible) {
		this.setState({ modalVisible: visible });
	}

	getHomeData = () => {
		let { pageNo, completedOrders, isLoading,
			totalOrders, renderStatus, loadingMore,
			undeliveredOrders, isRefreshing } = this.state;
		if (!Constants.is_available) {
			this.setState({ isLoading: false })
			Toast.showWithGravity("Sorry, you are offline", Toast.SHORT, Toast.BOTTOM);
			return
		}
		ApiManager.homeData({ status: renderStatus }, pageNo).then((res) => {
			// console.log(JSON.stringify(res.data.orders));
			if (res.code && res.code.code == 401) {
				NavigationService.navigate("Login");
			} else if (res.singleStringMessage) {
				setTimeout(() => {
					this.setState({
						isLoading: false,
						isRefreshing: false,
						loadingMore: false,
					}, () => console.log("loading paused"));
				}, 100)
				setTimeout(() => {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				}, 400)
			} else {
				// console.log(JSON.stringify(res));
				setTimeout(() => {
					this.setState({
						completedOrder: res.data.orderCounts.completed,
						totalOrder: res.data.orderCounts.total,
						undeliveredOrder: res.data.orderCounts.unDelivered,
						loadingMore: false,
						isLoading: false,
						isRefreshing: false
					}, () => console.log("loadingMore:-" + loadingMore + ",isLoading:-" + isLoading + ",isRefreshing:-" + isRefreshing))
				}, 10)
				this.setState({
					isListEnd: res.data.orders.length < 10 ? false : true,
					loadingMore: false,
					isLoading: false,
					isRefreshing: false,
					orders: pageNo === 1 ? res.data.orders : [...orders, ...res.data.orders],
				}, () => {
					console.log("loadingMore:-" + loadingMore + ",isLoading:-" + isLoading + ",isRefreshing:-" + isRefreshing)
					this.setOrders(res.data.orders)
				});
			}
		}).catch((err) => {
			this.setState({ isLoading: false });
			console.log("error" + JSON.stringify(err));
		})
	}

	startTimer = () => {
		Constants.timerId = setInterval(() => {
			// this.setState({
			// 	totalOrders: []
			// }, () => {
			// 	console.log("callling timer");
			// 	clearInterval(Constants.timerId);
			// 	this.setOrders(this.state.orders);
			// });
		}, 1000);
	}

	clearTimer = () => {
		if (Constants.timerId) {
			clearInterval(Constants.timerId);
		}
	}

	setOrders = (orders) => {
		let { pageNo, completedOrders, isLoading,
			totalOrders, renderStatus, loadingMore,
			undeliveredOrders, isRefreshing } = this.state;
		if (renderStatus == 1) {
			let orders1 = [];
			for (let i = 0; i < orders.length; i++) {
				if (orders[i].status == 6) {
					orders1.push(orders[i]);
				}
			}
			if (orders1.length) {
				DeviceEventEmitter.emit(Constants.WATCH_LOCATION);
			}
			this.props.saveDeliveryOrders(orders1);
			this.updateLocationInOrders();
			this.setState({
				totalOrders: pageNo === 1 ? orders : [...totalOrders, ...orders],
				deliveryOrders: orders,
				isListEnd: false,
				loadingMore: false,
				isLoading: false,
				isRefreshing: false
			}, () => {
				console.log("loadingMore:-" + loadingMore + ",isLoading:-" + isLoading + ",isRefreshing:-" + isRefreshing);
				if (this.state.orders.length) {
					this.startTimer();
				}
			})
		} else if (renderStatus == 3) {
			this.clearTimer();
			this.setState({
				completedOrders: pageNo === 1 ? orders : [...completedOrders, ...orders],
				isListEnd: false,
				loadingMore: false,
				isLoading: false,
				isRefreshing: false
			}, () => console.log("loadingMore:-" + loadingMore + "isLoading:-" + isLoading + "isRefreshing:-" + isRefreshing))
		} else {
			this.clearTimer();
			this.setState({
				undeliveredOrders: pageNo === 1 ? orders : [...undeliveredOrders, ...orders],
				isListEnd: false,
				loadingMore: false,
				isLoading: false,
				isRefreshing: false
			}, () => console.log("loadingMore:-" + loadingMore + "isLoading:-" + isLoading + "isRefreshing:-" + isRefreshing))
		}
	}

	handleLoadMore = () => {
		if (!this.state.loadingMore && !this.state.isListEnd) {
			this.setState({
				loadingMore: true,
				pageNo: this.state.pageNo + 1
			}, () => {
				this.getHomeData();
			});
		}
	};

	refreshOrder = () => {
		if (!this.state.isLoading && !this.state.isRefreshing) {
			this.setState({
				pageNo: 1,
				isRefreshing: true
			}, () => {
				this.getHomeData();
			})
		}
	}

	renderFooter = () => {
		let { isListEnd, loadingMore } = this.state;
		if (!loadingMore || isListEnd) {
			console.log("loadingMore:-" + loadingMore)
			return null;
		}
		return (
			<View
				style={{
					width: "100%",
					height: 50,
					marginTop: 10,
					marginBottom: 10,
					borderColor: Constants.lightgreen,
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<ActivityIndicator
					color={Constants.green}
					animating
					size="large" />
			</View>
		);
	}

	renderOrder = (totalOrder) => {
		let { renderStatus } = this.state;
		return (
			<View style={{ width: "95%", marginRight: "2.5%" }}>
				<View cardElevation={5}
					cardMaxElevation={5}
					cornerRadius={5}
					style={OrderStyles.orderBox}>
					{(totalOrder.is_express_delivery && renderStatus == 1) ?
						<Instant order={totalOrder}></Instant> : null}
					<View style={OrderStyles.firstView}>
						<View style={OrderStyles.firstInfoView}>
							<View style={OrderStyles.imageView}>
								{
									totalOrder.customer.picture ?
										<Image style={[OrderStyles.firstInfoImage, {
											borderColor: 'lightgray',
											borderWidth: 2,
										}]}
											resizeMode="cover" source={{ uri: totalOrder.customer.picture }} /> :
										<Image style={OrderStyles.firstInfoImage} source={Images.ImgSunny} />
								}
								{
									renderStatus == 1 ?
										<TouchableOpacity style={OrderStyles.callingButton} onPress={() => {
											const args = {
												number: totalOrder.customer.contact_number,
												prompt: false,
											};
											call(args).catch(console.error);
										}}>
											<Image style={{ width: 30, height: 30 }} resizeMode="contain" source={Images.ImgCall} />
										</TouchableOpacity> : null
								}
							</View>
							<View style={{ marginTop: 25, paddingLeft: 15, width: "70%" }}>
								<Text style={{
									fontFamily: Fonts.bold,
									color: Constants.black, fontSize: 16
								}} numberOfLines={1}>{totalOrder.address.delivery.full_name}</Text>
								<View style={{
									flexDirection: "row"
								}}>
									<Image source={Images.ImgClock} style={{ width: 12, height: 12, margin: 3, marginTop: 1 }}></Image>
									<Text style={{ color: 'gray', fontSize: 12, fontFamily: Fonts.medium }}>
										{moment(totalOrder.deliver_start_time).format("ha ")}-
																{moment(totalOrder.deliver_end_time).format(" ha")}
									</Text>
								</View>
							</View>
						</View>
						{
							(renderStatus != 3) ?
								<TouchableOpacity disabled={(renderStatus != 1)}
									style={[OrderStyles.actionButton, {
										backgroundColor: renderStatus == 1 ? Constants.green : "lightgrey"
									}]} onPress={() => {
										this.showAlertView(totalOrder);
									}}>
									<Text style={[OrderStyles.actionButtonText, {
										color: renderStatus == 1 ? "white" : Constants.black
									}]}>CHANGE STATUS</Text>
								</TouchableOpacity> :
								<View style={{ width: "35%" }}></View>
						}
					</View>
					<View style={OrderStyles.secondPanel}>
						<TouchableOpacity style={OrderStyles.secondPanelParts}
							onPress={() => NavigationService.navigate("OrderDetail", { order: totalOrder })}>
							<Text style={OrderStyles.secondpanelPartInfo}>Order ID</Text>
							<Text style={{ fontFamily: Fonts.bold, fontSize: 12, color: Constants.black, textTransform: "uppercase" }}>#{totalOrder.order_id}</Text>
						</TouchableOpacity>
						<View style={OrderStyles.secondPanelVerticalBorder} />
						<TouchableOpacity style={OrderStyles.secondPanelParts} disabled={(renderStatus != 1)}
							onPress={() => this.launchMap(totalOrder.address.pickup, 2)}>
							<View style={{ width: "100%", flexDirection: "row", justifyContent: "center" }}>
								<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Shop </Text>
								{
									(renderStatus == 1) ?
										<Image style={{ width: 20, height: 20, marginLeft: 10, resizeMode: "cover" }} source={Images.ImgDirectionTwo} /> : null
								}
							</View>
							<Text style={{
								marginTop: 1, fontFamily: Fonts.bold,
								fontSize: 12, color: Constants.black, textAlign: "center"
							}}>{totalOrder.store.name}</Text>
						</TouchableOpacity>
						<View style={OrderStyles.secondPanelVerticalBorder} />
						<View style={OrderStyles.secondPanelParts}>
							<Text style={OrderStyles.secondpanelPartInfo}>Payment Status </Text>
							<Text style={{
								marginTop: 1, fontFamily: Fonts.bold,
								fontSize: 12, color: totalOrder.payment_type == 2 ? Constants.green : Constants.gold,
								// marginLeft: 14
							}}>{totalOrder.payment_type == 1 ? "Pending" : "Paid"} </Text>
						</View>
					</View>
					<View style={OrderStyles.thirdPanel}>
						<View style={OrderStyles.thirdPanelView}>
							<TouchableOpacity disabled={(renderStatus != 1)} style={OrderStyles.pickerButton} onPress={() => this.launchMap(totalOrder.address.delivery, 1)}>
								<Image style={{ width: 18, height: 18 }} resizeMode="cover" source={Images.ImgLocation} />
							</TouchableOpacity>
							<TouchableOpacity disabled={(renderStatus != 1)} style={OrderStyles.pickerButtonView}
								onPress={() => this.launchMap(totalOrder.address.delivery, 1)}>
								<Text style={OrderStyles.pickerButtonText}>{totalOrder.address.delivery.house_no} {totalOrder.address.delivery.landmark} {totalOrder.address.delivery.locality} {totalOrder.address.delivery.city.name}
								</Text>
								{
									(renderStatus == 1) ?
										<Image style={{ width: 20, height: 20, marginLeft: 10 }} resizeMode="cover" source={Images.ImgDirectionTwo} /> : null
								}
							</TouchableOpacity>
							<View style={OrderStyles.priceView}>
								<Text style={OrderStyles.priceViewText}>{Constants.currency} {(totalOrder.total_amount_after_tax + totalOrder.delivery_charges) - totalOrder.discount}</Text>
							</View>
						</View>
					</View>
				</View>
			</View>
		)
	}

	keyExtractor = (item) => item._id;

	launchMap = (data, type) => {
		// let address = "Huda City Metro, Gurugram";
		let address = "";
		let coords = [];
		if (data.coordinates) {
			coords.push(data.coordinates.latitude);
			coords.push(data.coordinates.longitude);
		} else {
			if (type == 1) {
				if (data.gps_address) {
					address = data.gps_address;
				} else {
					address += "House no. " + data.house_no + ', ' + data.locality + ", " + data.landmark + ', ' + data.city.name;
				}
			} else {
				address += "Shop no. " + data.shop_no + ', ' + data.locality + ', ' + data.city.name;
			}
		}
		if (address.length || coords.length) {
			LaunchNavigator.navigate(coords.length ? coords : address)
				.then(() => console.log("Launched navigator"))
				.catch((err) => console.error("Error launching navigator: " + err));
		} else {
			Toast.showWithGravity("Address not specified...", Toast.SHORT, Toast.BOTTOM);
		}
	}

	renderOrderList = () => {
		let { isRefreshing, renderStatus, completedOrders, undeliveredOrders, totalOrders } = this.state;
		let orders = [];
		switch (renderStatus) {
			case 1:
				orders = totalOrders
				break;
			case 4:
				orders = undeliveredOrders
				break;
			case 3:
				orders = completedOrders
				break;
			default:
				orders = [];
				break;
		}
		return (
			<View style={{ width: '100%', flex: 1 }}>
				<FlatList data={orders}
					renderItem={({ item }) => this.renderOrder(item)}
					keyExtractor={this.keyExtractor}
					onEndReached={this.handleLoadMore}
					onEndReachedThreshold={1}
					onRefresh={this.refreshOrder}
					refreshing={isRefreshing} />
				{this.renderFooter()}
			</View>
		)
	}

	deleteOrderFromTracking = async () => {
		console.log("deleting order :--" + this.state.modalData.order_id);
		// alert("deleting order :--" + this.state.modalData.order_id);
		await FirebaseService.deleteOrder(this.props.getDriver._id, this.state.modalData.order_id);
		if (!this.props.getDeliveryOrders || !this.props.getDeliveryOrders.length) {
			DeviceEventEmitter.emit(Constants.CANCEL_WATCH_LOCATION);
		}
	}

	updateLocationInOrders = async () => {
		let orders = this.props.getDeliveryOrders;
		for (let i = 0; i < orders.length; i++) {
			let data = {};
			data.pickUpLatitude = orders[i].address.pickup.coordinates.latitude;
			data.pickUpLongitude = orders[i].address.pickup.coordinates.longitude;
			data.deliverLatitude = orders[i].address.delivery.coordinates.latitude;
			data.deliverLongitude = orders[i].address.delivery.coordinates.longitude;
			data.mLattitude = this.props.getLocation.latitude;
			data.mLongitude = this.props.getLocation.longitude;
			data.driverId = this.props.getDriver._id;
			data.order_id = orders[i].order_id;
			data.status = orders[i].status;
			// alert("Updating order index:--" + i + " and order id:--" + data.order_id);
			console.log("Updating order index:--" + i + " and order id:--" + data.order_id);
			await FirebaseService.insertDriver(data);
		}
	}

	// show alert modal 
	showAlertView = (order) => {
		let { modalVisible } = this.state;
		this.setState({ modalData: order, modalVisible: true }, () => {
			console.log("modalVisible:--" + modalVisible);
			console.log("order status is :--" + order.status);
		})
	}

	requestPermission = async (status) => {
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
				{
					'title': 'Location Permission',
					'message': 'App needs access to your location '
				}
			)
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				// Toast.showWithGravity("Location Permission Granted.", Toast.SHORT, Toast.BOTTOM);
				if (status) {
					this.setState({
						modalVisible: false, pageNo: 1,
						renderStatus: 1, isLoading: true
					}, () => {
						console.log("modal closed? " + this.state.modalVisible)
						this.getHomeData();
					})
				} else this.submitOrderStatus({ status: 6 });
			} else {
				if (status) {
					this.setState({
						modalVisible: false, pageNo: 1,
						renderStatus: 1, isLoading: true
					}, () => {
						console.log("modal closed? " + this.state.modalVisible)
						this.getHomeData();
					})
				} else this.requestAlert(granted);
				// Toast.showWithGravity("Location Permission Not Granted", Toast.SHORT, Toast.BOTTOM);
			}
		} catch (err) {
			if (status) {
				this.setState({
					modalVisible: false, pageNo: 1,
					renderStatus: 1, isLoading: true
				}, () => {
					console.log("modal closed? " + this.state.modalVisible)
					this.getHomeData();
				})
			}
			console.warn(err);
			Toast.showWithGravity("Location Permission Error", Toast.SHORT, Toast.BOTTOM);
		}
	}

	requestAlert = (granted) => {
		let messsage = "Please change the location permission as 'Always', we need rider location to track the grocery orders.";
		const isIOS = Platform.OS == "ios";
		// if (granted) {
		// 	messsage = "Your location permission is as '" + granted + "', but we need the location services for tracking the order";
		// }
		Alert.alert('', messsage, [
			{
				text: 'CANCEL',
				onPress: () => console.log('Cancel Pressed'),
				style: 'cancel',
			},
			{
				text: 'ENABLE', onPress: () => {
					if (isIOS) {
						// this.requestPermissionIos();
						Linking.canOpenURL('app-settings:').then(supported => {
							if (!supported) {
								console.log('Can\'t handle settings url');
							} else {
								Linking.openURL('app-settings:');
							}
						}).catch(err => console.error('An error occurred', err));
					} else {
						// AndroidOpenSettings.locationSourceSettings();
						AndroidOpenSettings.appDetailsSettings();
					}
				}
			},
		],
			{ cancelable: false }
		)
	}

	markIncompleteFunction = () => {
		console.log("completed")
		this.setState({ markcomplete: false })
	}

	closeUndeliverOrderModal = () => {
		console.log("CLOSE CALL")
		this.setState({ markcomplete: true })
	}

	checkPermission = (status) => {
		if (Platform.OS === 'android') {
			this.requestPermission(status);
		} else {
			Geolocation.getCurrentPosition((position) => {
				console.log("first location fetched:---" + JSON.stringify(position));
				if (status) {
					this.setState({
						modalVisible: false, pageNo: 1,
						renderStatus: 1, isLoading: true
					}, () => {
						console.log("modal closed? " + this.state.modalVisible)
						this.getHomeData();
					})
				} else this.submitOrderStatus({ status: 6 });
			}, (error) => {
				console.log(JSON.stringify(error));
				// console.log(error.code, error.message);
				if (status) {
					this.setState({
						modalVisible: false, pageNo: 1,
						renderStatus: 1, isLoading: true
					}, () => {
						console.log("modal closed? " + this.state.modalVisible)
						this.getHomeData();
					})
				} else this.requestAlert();
			});
		};
	}

	markCompleteFunction = () => {
		console.log("completed")
		let status = 2;
		console.log("modalData status is :--" + this.state.modalData.status);
		if (this.state.modalData.status == 2) {
			this.checkPermission();
		} else {
			switch (this.state.modalData.status) {
				case 2:
					status = 6
					break;
				case 6:
					status = 3
					break;
				default:
					status = 2;
					break;
			}
			this.submitOrderStatus({ status: status });
		}
	}

	submitOrderStatus = (data) => {
		if (!Constants.is_available) {
			Toast.showWithGravity("Sorry, you are offline", Toast.SHORT, Toast.BOTTOM);
			return
		}
		this.setState({ modalLoading: true });
		console.log("data is :--" + JSON.stringify(data));
		ApiManager.changeOrderStatus(this.state.modalData._id, data).then((res) => {
			setTimeout(() => {
				this.setState({ modalLoading: false }, () => console.log("modal loading paused"));
			}, 100)
			setTimeout(() => {
				if (res.code && res.code.code == 401) {
					NavigationService.navigate("Login");
				}
				if (res.singleStringMessage) {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				} else {
					let orders = this.state.deliveryOrders;
					if (this.state.modalData.status == 2) {
						orders.push(this.state.modalData);
						this.props.saveDeliveryOrders(orders);
					} else if (this.state.modalData.status == 6) {
						let index = -1;
						for (let i = 0; i < orders.length; i++) {
							if (orders[i]._id == this.state.modalData._id) {
								index = i;
								break;
							}
						}
						if (index >= 0) {
							this.deleteOrderFromTracking();
							// orders.splice(index);
							// this.setState({ totalOrders: orders });
							this.props.saveDeliveryOrders(orders);
						}
					}
					this.setState({
						modalVisible: false,
						markcomplete: true,
						pageNo: 1,
						loading: true
					}, () => this.getHomeData());
				}
			}, 400);
		}).catch((err) => {
			this.setState({ modalLoading: false }, () => console.log("modal loading paused"));
		})
	}

	submitReason = (data) => {
		if (!Constants.is_available) {
			Toast.showWithGravity("Sorry, you are offline", Toast.SHORT, Toast.BOTTOM);
			return
		}
		let reason = "";
		switch (data.reasonIndex) {
			case 1:
				reason += "Location is not Correct";
				break;
			case 2:
				reason += "Not able to Contact to Customer";
				break;
			case 3:
				reason += data.reason;
				break;
			default: reason = "N/A"
				break;
		}
		this.submitOrderStatus({
			status: 4,
			undelivered_description: reason
		});
	}

	changeTab = (status) => {
		this.setState({
			renderStatus: status, pageNo: 1,
			isLoading: true, totalOrders: [],
			undeliveredOrders: [],
			completedOrders: []
		}, () => {
			this.getHomeData()
		})
	}

	render() {
		let {
			isLoading, renderStatus, undeliveredOrder,
			totalOrder, completedOrder, modalLoading, modalData,
			modalVisible, markcomplete, totalOrders
		} = this.state;
		return (
			<View style={{ flex: 1, backgroundColor: Constants.backgroundScreenColor }}>
				{
					(renderStatus == 1 && totalOrders.length > 0) ?
						<ChangeOrderStatusPopup
							modalLoading={modalLoading}
							modalData={modalData}
							modalVisible={modalVisible}
							markcomplete={markcomplete}
							submitReasonFunction={this.submitReason}
							closeModel={() => {
								this.setState({ modalVisible: false }, () => {
									console.log("CLOSED:")
								})
							}}
							visibilityfunction={this.showAlertView.bind(this)}
							markIncompleteFunction={this.markIncompleteFunction.bind(this)}
							closeUndeliverOrderModal={this.closeUndeliverOrderModal.bind(this)}
							markCompleteFunction={this.markCompleteFunction.bind(this)}>
						</ChangeOrderStatusPopup> : null
				}
				<Spinner size="large"
					visible={isLoading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<NavigationEvents
					onWillFocus={() => {
						DeviceEventEmitter.addListener("CANCEL_NOTIFICATION", this.cancelNotificationHandler)
						console.log(this.props.navigation.state.routeName);
						this.checkPermission(true);
						setTimeout(() => {
							this.setState({ isLoading: false }, () => {
								console.log("forcefully isloading update to:--" + isLoading);
							})
						}, 4000)

						//Call whatever logic or dispatch redux actions and update the screen!
					}}

					onWillBlur={() => {
						DeviceEventEmitter.removeListener("CANCEL_NOTIFICATION", this.cancelNotificationHandler)

						//Call whatever logic or dispatch redux actions and update the screen!
					}}

				/>
				<ImageBackground resizeMode='cover' source={Images.ImgHeader} style={styles.Header}>
					<Text style={[styles.headerlabel]}>Today's Assigned Order</Text>
					<View style={styles.headerView}>
						<TouchableOpacity style={[styles.headerButton,
						{ borderBottomColor: renderStatus == 1 ? Constants.green : "white", }
						]} onPress={() => this.changeTab(1)}>
							<Text style={[styles.headerValue, { color: Constants.red, }]}>{totalOrder}</Text>
							<Text style={styles.headerButtonlabel}>Total Orders</Text>
						</TouchableOpacity>
						<RenderVerticalBorder />
						<TouchableOpacity style={[styles.headerButton, {
							borderBottomColor: renderStatus == 4 ? Constants.green : "white",
						}]} onPress={() => this.changeTab(4)}>
							<Text style={[styles.headerValue, { color: Constants.gold }]}>{undeliveredOrder}</Text>
							<Text style={styles.headerButtonlabel}>Undeliverd Orders</Text>
						</TouchableOpacity>
						<RenderVerticalBorder />
						<TouchableOpacity style={[styles.headerButton, {
							borderBottomColor: renderStatus == 3 ? Constants.green : "white",
						}]} onPress={() => this.changeTab(3)}>
							<Text style={[styles.headerValue, { color: '#4387f6' }]}>{completedOrder}</Text>
							<Text style={styles.headerButtonlabel}>Completed Orders</Text>
						</TouchableOpacity>
					</View>
				</ImageBackground>
				{/* <Text style={{ color: Constants.black, fontSize: 16, marginLeft: 10, marginTop: 10 }}>Total Orders Delivery</Text> */}
				{this.renderOrderList()}
				<StatusBar backgroundColor={Constants.green} barStyle="light-content" />
			</View >
		)
	}
}

const styles = StyleSheet.create({
	Header: {
		width: '100%',
		height: 200,
		alignItems: 'center'
	},
	headerView: {
		backgroundColor: 'white', height: 80,
		width: '95%', marginTop: 20,
		flexDirection: 'row', borderRadius: 5
	},
	headerlabel: {
		marginTop: 50, fontSize: 16,
		color: Constants.white, fontFamily: Fonts.bold
	},
	headerButton: {
		height: '100%', width: '33%',
		alignItems: 'center', borderWidth: 3,
		borderColor: 'white',
	},
	headerValue: {
		fontSize: 22,
		marginTop: 15,
		fontFamily: Fonts.bold,
		alignItems: "center"
	},
	headerButtonlabel: { fontSize: 12, color: Constants.black, marginTop: 10 }
});

const mapStateToProps = (state) => {
	return {
		getDriver: state.getDriver.user,
		getToken: state.getToken.token,
		getFcmToken: state.getFcmToken.fcmToken,
		getDeliveryOrders: state.getDeliveryOrders.orders,
		getLocation: state.getLocation.location,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		saveDeliveryOrders: (data) => dispatch(Actions.saveOrderToDelivery(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)