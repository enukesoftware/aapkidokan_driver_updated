import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, ActivityIndicator, Image, ScrollView,DeviceEventEmitter} from 'react-native';
import Images from '../../../Componants/Images';
import Constants from '../../../Componants/Constants';
import CardView from 'react-native-cardview';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import * as ApiManager from './../../../ApiManager/ApiManager';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import Fonts from '../../../Componants/Fonts';
import NavigationService from '../../../Componants/NavigationService';
import styles from './Styles';
import SInfo from 'react-native-sensitive-info';

export default class JobRequest extends Component {

	constructor(props) {
		super(props);
		this.state = {
			orders: [],
			pageNo: 1,
			isLoading: false,
			isRefreshing: false,
			isListEnd: false,
			loadingMore: false,
		};

		this.orderNotificationHandler = this.orderNotificationHandler.bind(this);
	}

	static navigationOptions = () => ({
		header: null
	});

	// componentDidMount() {
	// 	this.updateNotificationStatus()
	// }

	updateNotificationStatus = async () => {
		// await SInfo.deleteItem('notifications', Constants.sharedPreference);
		let notifications = await SInfo.getItem('notifications', Constants.sharedPreference);
		if (notifications) {
			let parsedData = JSON.parse(notifications)

			parsedData.forEach(element => {
				element["isNew"] = false
			});

			SInfo.setItem('notifications', JSON.stringify(parsedData), Constants.sharedPreference);
		}

	}

	getOrders = () => {
		let { pageNo, orders } = this.state;
		if (!Constants.is_available) {
			Toast.showWithGravity("Sorry, you are offline", Toast.SHORT, Toast.BOTTOM);
			this.setState({ isLoading: false });
			return
		} else {
			this.setState({ isLoading: true });
			ApiManager.getOrdersToAccept(pageNo).then((res) => {
				setTimeout(() => {
					this.setState({
						isLoading: false,
						isRefreshing: false,
					}, () => console.log("loading paused"));
				}, 100)
				setTimeout(() => {
					if (res.code && res.code.code == 401) {
						NavigationService.navigate("Login");
					}
					if (res.singleStringMessage) {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
					} else {
						this.setState({
							isListEnd: (!res.data.orders.length || !res.data.orders.length < 10),
							loadingMore: false,
							orders: pageNo === 1 ? res.data.orders : [...orders, ...res.data.orders],
						}, () => console.log("data has been set into state"));
					}
				}, 300);
			}).catch(() => {
				this.setState({ isLoading: false }, () => console.log("loading paused"));
			});
		}
	}

	handleLoadMore = () => {
		if (!this.state.loadingMore && !this.state.isListEnd) {
			this.setState({
				loadingMore: true,
				pageNo: this.state.pageNo + 1
			}, () => {
				this.getOrders();
			});
		}
	};

	refreshOrder = () => {
		if (!this.state.isLoading && !this.state.isRefreshing) {
			this.setState({
				pageNo: 1,
				isRefreshing: true
			}, () => {
				this.getOrders();
			})
		}
	}

	renderFooter = () => {
		let { isListEnd, loadingMore } = this.state;
		if (!loadingMore || isListEnd) {
			console.log("loadingMore:-" + loadingMore)
			return null;
		}
		return (
			<View
				style={{
					width: "100%",
					height: 50,
					marginTop: 10,
					marginBottom: 10,
					borderColor: Constants.lightgreen,
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<ActivityIndicator
					color={Constants.green}
					animating
					size="large" />
			</View>
		);
	}

	acceptOrder = (id) => {
		if (!Constants.is_available) {
			Toast.showWithGravity("Sorry, you are offline", Toast.SHORT, Toast.BOTTOM);
			return
		}
		this.setState({ isLoading: true });
		ApiManager.acceptOrder(id).then((res) => {
			setTimeout(() => {
				this.setState({ isLoading: false }, () => console.log("loading paused"));
			}, 100)
			setTimeout(() => {
				if (res.code && res.code.code == 401) {
					NavigationService.navigate("Login");
				}
				if (res && res.success) {
					Toast.showWithGravity("Order has been accepted", Toast.SHORT, Toast.BOTTOM)
					this.setState({
						pageNo: 1
					}, () => this.getOrders());
				} else {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM)
				}
			}, 500);
		}).catch(() => {
			this.setState({ isLoading: false }, () => console.log("loading paused"));
		});
	}

	renderOrder = (totalOrder) => {
		return (
			<CardView cardElevation={5}
				cardMaxElevation={5} cornerRadius={5}
				style={styles.orderBox}>
				<View style={{ height: '40%', width: '100%', flexDirection: 'row' }}>
					<View style={{ height: '100%', width: '80%', flex: 1, flexDirection: 'row' }}>
						<View style={{
							width: 60, height: 60, borderRadius: 30,
							borderColor: 'lightgray',
							borderWidth: 5,
							marginTop: 10, marginLeft: 10,
							justifyContent: "center",
							alignItems: "center"
						}}>
							{
								(totalOrder.customer && totalOrder.customer.picture) ?
									<Image style={{ width: 56, height: 56, borderRadius: 30, }}
										resizeMode="cover" source={{ uri: totalOrder.customer.picture }} /> :
									<Image style={{ width: 56, height: 56, borderRadius: 30, }} resizeMode="cover" source={Images.ImgSunny} />
							}
						</View>
						<View style={{ marginTop: 25, marginLeft: 15 }}>
							<Text style={{ fontFamily: Fonts.bold, color: Constants.black, fontSize: 16 }}>{totalOrder.address.delivery.full_name}</Text>
							<View style={{
								flexDirection: "row"
							}}>
								<Image source={Images.ImgCalendar} style={{ width: 12, height: 12, margin: 3, marginTop: 1 }}></Image>
								<Text style={{ color: 'gray', fontSize: 12, fontFamily: Fonts.medium }}>{moment(totalOrder.deliver_end_time).format("D MMM, YYYY")}</Text>
								<Text style={{ color: 'gray', fontSize: 12, fontFamily: Fonts.medium }}>, {moment(totalOrder.deliver_start_time).format("ha ")}-
																{moment(totalOrder.deliver_end_time).format(" ha")}</Text>
							</View>
						</View>
					</View>
					<TouchableOpacity style={{
						height: 30, alignItems: 'center',
						backgroundColor: Constants.green,
						width: 70, marginRight: 15, borderRadius: 15, marginTop: 30
					}} onPress={() => this.acceptOrder(totalOrder._id)}>
						<Text style={{
							fontSize: 12,
							color: 'white', marginTop: 8, fontFamily: Fonts.bold
						}}>ACCEPT</Text>
					</TouchableOpacity>
				</View>
				<View style={{ height: '25%', width: '100%', flexDirection: 'row', marginTop: 10 }}>
					<TouchableOpacity onPress={() => NavigationService.navigate("OrderDetail", { order: totalOrder })} style={{ height: '100%', width: '33%', alignItems: "center" }}>
						<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Order ID</Text>
						<Text style={{ fontFamily: Fonts.bold, fontSize: 12, color: Constants.black, marginLeft: 14, textTransform: "uppercase" }}>#{totalOrder.order_id}</Text>
					</TouchableOpacity>
					<View style={{ backgroundColor: 'lightgray', height: '100%', width: 1 }}>
					</View>
					<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
						<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Shop </Text>
						<Text style={{
							marginTop: 1, fontFamily: Fonts.bold,
							fontSize: 12, color: Constants.black, textAlign: "center"
						}}>{totalOrder.store ? totalOrder.store.name : "N/A"}</Text>
					</View>
					<View style={{ backgroundColor: 'lightgray', height: '100%', width: 1 }}>
					</View>
					<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
						<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Payment Status </Text>
						<Text style={{
							marginTop: 1, fontFamily: Fonts.bold,
							fontSize: 12, color: totalOrder.payment_type == 2 ? Constants.green : Constants.gold,
							marginLeft: 14
						}}>{totalOrder.payment_type == 1 ? "Pending" : "Paid"} </Text>
					</View>
				</View>
				<View style={{ backgroundColor: 'lightgray', height: 1, width: '100%', marginTop: 10 }}>
				</View>
				<View style={{ height: '25%', width: '100%', flexDirection: 'row' }}>
					<View style={{
						height: '100%',
						width: '80%',
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "space-between"
					}}>
						<View style={{
							width: "auto",
							maxWidth: "100%",
							height: "100%",
							flexDirection: "row",
							alignItems: "center",
							justifyContent: "flex-start",
						}}>
							<View style={{ width: 30, height: "100%", alignItems: "center", justifyContent: "center" }}>
								<Image style={{ width: 18, height: 18 }} resizeMode="cover" source={Images.ImgLocation} />
							</View>
							<View style={{
								width: 'auto', height: "100%",
								flexDirection: 'row', alignItems: "center",
							}}>
								<Text style={{
									color: Constants.black,
									fontSize: 14,
									width: "80%", fontFamily: Fonts.medium
								}} numberOfLines={2}>{totalOrder.address.delivery.house_no} {totalOrder.address.delivery.landmark} {totalOrder.address.delivery.locality}</Text>
							</View>
						</View>
						<View style={{
							width: "20%",
							minWidth: 100,
							alignItems: "flex-end", paddingRight: 10,
							height: "100%", justifyContent: "center",
						}}>
							<Text style={{
								fontFamily: Fonts.bold,
								color: Constants.black, fontSize: 14, textAlign: "center"
							}}>{Constants.currency} {(totalOrder.total_amount_after_tax + totalOrder.delivery_charges) - totalOrder.discount}</Text>
						</View>
					</View>
				</View>
			</CardView>
		)
	}

	keyExtractor = (item) => item._id;

	handleLoadMore = () => {
		if (!this.state.loadingMore && !this.state.isListEnd) {
			this.setState({
				loadingMore: true,
				pageNo: this.state.pageNo + 1
			}, () => {
				this.getOrders();
			});
		}
	};

	refreshOrder = () => {
		if (!this.state.isLoading && !this.state.isRefreshing) {
			this.setState({
				pageNo: 1,
				isRefreshing: true
			}, () => {
				this.getOrders();
			})
		}
	}

	renderOrderList = () => {
		let { orders, isRefreshing } = this.state;
		return (
			<View style={{ width: '100%', flex: 1 }}>
				<FlatList data={orders}
					renderItem={({ item }) => this.renderOrder(item)}
					keyExtractor={this.keyExtractor}
					onEndReached={this.handleLoadMore}
					onEndReachedThreshold={1}
					onRefresh={this.refreshOrder}
					refreshing={isRefreshing}
					ListEmptyComponent={this.renderEmptyPage} />
				{this.renderFooter()}
			</View>
		)
	}

	renderEmptyPage = () => {
		return (
			<View style={{
				// backgroundColor: "red",
				justifyContent: "center",
				alignItems: "center",
				width: "100%", height: Constants.height - 100
			}}>
				<Text>No jobs found</Text>
			</View>
		)
	}

	orderNotificationHandler() {
		console.log("EVENT")
		this.setState({
			pageNo: 1,
			isRefreshing: false,
			isLoading: true
		}, () => {
			this.getOrders();
			this.updateNotificationStatus()
		})
	}

	render() {
		let { isLoading, pageNo, isRefreshing } = this.state;
		return (
			<View style={{ flex: 1 }}>
				<NavigationEvents 
				onWillFocus={() => {
					DeviceEventEmitter.addListener("ORDER_NOTIFICATION", this.orderNotificationHandler)
					console.log(this.props.navigation.state.routeName);
					this.setState({
						pageNo: 1,
						isRefreshing: false,
						isLoading: true
					}, () => {
						this.getOrders();
						this.updateNotificationStatus()
					})
					//Call whatever logic or dispatch redux actions and update the screen!
				}}

				onWillBlur={() => {
					DeviceEventEmitter.removeListener("ORDER_NOTIFICATION", this.orderNotificationHandler)
				}}
				/>
				<Spinner size="large"
					visible={isLoading && pageNo == 1 && !isRefreshing}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<View style={styles.statusbar}></View>
				<View style={styles.headerView}>
					<TouchableOpacity style={styles.refreshButton}
						onPress={() => {
							this.setState({
								pageNo: 1,
								isRefreshing: false,
								isLoading: true
							}, () => {
								this.getOrders();
							})
						}}>
						<Image source={Images.ImgRefresh} style={{ width: 20, height: 20 }} resizeMode="contain"></Image>
					</TouchableOpacity>
					<Text style={styles.headertext}>My Order Job Request</Text>
				</View>
				<View style={{ flex: 1, backgroundColor: Constants.backgroundScreenColor }}>
					{this.renderOrderList()}
				</View>
			</View>
		)
	}
}