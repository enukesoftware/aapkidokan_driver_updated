import { StyleSheet } from 'react-native';
import Constants from '../../../Componants/Constants';
import Fonts from '../../../Componants/Fonts';

export default StyleSheet.create({
	Header: {
		width: '100%',
		height: 200,
		alignItems: 'center'
	},
	orderBox: {
		marginTop: 10,
		backgroundColor: "white",
		height: 200,
		width: '95%',
		borderRadius: 10,
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: .8,
		shadowRadius: 2,
		elevation: 1,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: '2.5%',
		marginBottom: 10
	},
	headerlabel: {
		marginTop: 50, fontSize: 16,
		color: Constants.white, fontFamily: Fonts.bold
	},
	statusbar: {
		backgroundColor: Constants.green,
		width: "100%",
		height: Constants.statusBarHeight
	},
	headerView: {
		backgroundColor: Constants.green,
		width: "100%", height: 50,
		alignItems: "center",
		justifyContent: "center"
	},
	refreshButton: {
		position: "absolute", right: 10, bottom: 5,
		width: 30, height: 30, justifyContent: "center", alignItems: "center"
	},
	headertext: {
		fontFamily: Fonts.bold, color: 'white',
		fontSize: 18,
	}
});