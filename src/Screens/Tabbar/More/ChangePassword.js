import React, { Component } from 'react';
import { View, Text, findNodeHandle, ScrollView, Image, StyleSheet, Dimensions, TouchableOpacity, Animated, Alert, Platform, TextInput } from 'react-native';
import Images from '../../../Componants/Images';
import Constants from '../../../Componants/Constants';
import Spinner from 'react-native-loading-spinner-overlay';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import Toast from 'react-native-simple-toast';
import * as ApiManager from '../../../ApiManager/ApiManager';
import Fonts from '../../../Componants/Fonts';
import NetInfo from "@react-native-community/netinfo";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import NavigationService from '../../../Componants/NavigationService';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'

export default class ChangePasswordScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			user: Constants.user,
			headerHeight: 0,
			new_password: "",
			old_password: "",
			re_password: "",
			loading: false
		};
	}

	changePassword = async () => {
		const netInfo = await NetInfo.fetch();
		if (!netInfo.isConnected) {
			Toast.showWithGravity(
				"Internet is not available",
				Toast.LONG,
				Toast.BOTTOM //TOP,BOTTOM
			);
			return
		}
		if (!this.state.old_password.length) {
			Toast.showWithGravity(
				"Please enter your old password ...",
				Toast.SHORT,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.new_password.length) {
			Toast.showWithGravity(
				"Please enter your new password ...",
				Toast.SHORT,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if (!this.state.re_password.length) {
			Toast.showWithGravity(
				"Please confirm your new password ...",
				Toast.SHORT,
				Toast.CENTER //TOP,BOTTOM
			);
		} else if (this.state.new_password != this.state.re_password) {
			Toast.showWithGravity(
				"Password mismatch ...",
				Toast.SHORT,
				Toast.CENTER //TOP,BOTTOM
			);
		} else {
			this.setState({ loading: true })
			ApiManager.changePassword({
				password: this.state.old_password,
				new_password: this.state.new_password
			}).then((res) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => {
						console.log("loading paused");
					})
				}, 100);
				setTimeout(() => {
					Toast.showWithGravity(
						(res && res.success) ? res.data.message : res.singleStringMessage,
						Toast.LONG,
						Toast.CENTER //TOP,BOTTOM
					);
					if (res.code && res.code.code == 401) {
						NavigationService.navigate("Login");
					} else if (res && res.success) {
						NavigationService.goBack();
					}
				}, 1000);
			}).catch((err) => {
				setTimeout(() => {
					this.setState({ loading: false }, () => {
						console.log("loading paused");
					})
				}, 100);
				setTimeout(() => {
					Alert.alert(JSON.stringify(err));
				}, 1000);
			})
		}
	}

	renderPage = () => {
		return (
			<KeyboardAwareView animated={true}>
				<View style={{
					flex: 1,
					paddingTop: (this.state.headerHeight - 15),
					alignItems: 'center',
					justifyContent: 'flex-start',
					backgroundColor: Constants.backgroundPageColor,
					paddingTop: this.state.screenHeight / 7
				}}>
					< ScrollView style={{ width: "100%" }}
						showsVerticalScrollIndicator={false}
						keyboardShouldPersistTaps={'handled'}
						ref="myScrollView" keyboardDismissMode='interactive'
						contentContainerStyle={{
							height: 300, width: "100%",
							paddingLeft: 17,
							paddingRight: 17,
							alignItems: "center",
						}}>
						<View style={[styles.inputSection, { width: "90%" }]}>
							<Image source={Images.ImgPassword} style={styles.inputLogo} />
							<TextInput
								style={styles.input}
								value={this.state.old_password}
								onChangeText={(text) => { this.setState({ old_password: text }) }}
								placeholder='Old Password'
								placeholderTextColor={Constants.placeHolderColor}
								secureTextEntry={true}
								returnKeyType="next"
								autoCapitalize="none"
								ref="myInput"
								onFocus={this.inputFocused.bind(this, 'myInput')}
							/>
						</View>
						<View style={[styles.inputSection, { width: "90%" }]}>
							<Image source={Images.ImgPassword} style={styles.inputLogo} />
							<TextInput
								style={styles.input}
								value={this.state.new_password}
								onChangeText={(text) => { this.setState({ new_password: text }) }}
								placeholder='New Password'
								placeholderTextColor={Constants.placeHolderColor}
								secureTextEntry={true}
								returnKeyType="next"
								autoCapitalize="none"
								ref="myInput1"
								onFocus={this.inputFocused.bind(this, 'myInput1')}
							/>
						</View>
						<View style={[styles.inputSection, { width: "90%" }]}>
							<Image source={Images.ImgPassword} style={styles.inputLogo} />
							<TextInput
								style={styles.input}
								value={this.state.re_password}
								onChangeText={(text) => { this.setState({ re_password: text }) }}
								placeholder='Confirm Password'
								placeholderTextColor={Constants.placeHolderColor}
								secureTextEntry={true}
								returnKeyType="done"
								autoCapitalize="none"
								ref="myInput2"
								onFocus={this.inputFocused.bind(this, 'myInput2')}
								onSubmitEditing={() => this.changePassword()}
							/>
						</View>
						<TouchableOpacity style={{
							backgroundColor: Constants.green,
							padding: 10,
							marginLeft: 17,
							marginRight: 17,
							borderRadius: 25,
							marginTop: 50
						}} onPress={() => this.changePassword()}>
							<Text style={{ color: "white", fontSize: 16, fontFamily: Fonts.bold }}>CHANGE PASSWORD</Text>
						</TouchableOpacity>
					</ScrollView>
				</View>
			</KeyboardAwareView>
		)
	}

	inputFocused = (ref) => {
		this._scroll(ref, ref == "myInput2" ? 300 : 180);
	}

	_scroll = (ref, offset) => {
		setTimeout(() => {
			var scrollResponder = this.refs.myScrollView.getScrollResponder();
			scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
				findNodeHandle(this.refs[ref]),
				offset,
				true
			);
		}, 100);
	}

	render() {
		console.log("More page");
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner
					size="large"
					visible={this.state.loading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<Animated.View
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderPage()}
				</Animated.View>
				<Animated.View style={{
					top: 0, left: 0,
					right: 0, position: 'absolute',
					flexDirection: 'row',
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: {
							height: (getStatusBarHeight(true) + 40),
						}
					}),
					backgroundColor: '#1faf4b',
					// backgroundColor: 'blue',
					paddingBottom: 10
				}}>
					<Animated.View style={{
						height: '100%',
						width: 60,
						minWidth: 55,
						minHeight: 55,
						justifyContent: 'center',
						alignItems: 'center',
						position: "absolute",
						left: 0,
						top: 0,
						zIndex: 2,
						paddingLeft: 15
					}}>
						<TouchableOpacity style={{
							height: 45, width: 45,
							...Platform.select({
								ios: { marginTop: 50 },
								android: { marginTop: 19 }
							}),
							// backgroundColor: "red"
						}} underlayColor='transparent'
							onPress={() => {
								this.props.navigation.goBack()
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.ImgBack} />
						</TouchableOpacity>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						paddingLeft: 60,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						// alignItems: "center",
						width: '100%',
						// backgroundColor: 'blue',
					}} onLayout={(event) => {
						this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
							console.log("Header height is:--" + this.state.headerHeight);
						})
						console.log(event.nativeEvent.layout);
					}}>
						<Text style={{
							fontSize: 16,
							color: 'white',
							fontWeight: '500', width: 137
						}}>Change Password</Text>
					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	bgImage: {
		width: "100%",
		height: "100%",
		position: "absolute"
	},
	outerSection: {
		flex: 1,
		// height: (Dimensions.get("window").height),
		alignItems: 'center',
		justifyContent: 'center',
		position: "relative"
	},
	innerSection: {
		backgroundColor: 'white',
		alignItems: 'center',
		shadowOpacity: 1,
		shadowRadius: 3,
		shadowOffset: { height: 0, width: 0 },
		elevation: 1, zIndex: 0, borderRadius: 10,
		width: ((Dimensions.get("window").width) - 50),
		minHeight: 480,
		position: "relative",
		marginTop: 45,
		paddingTop: 50,
		marginBottom: 0
	},
	innerImage: {
		width: 100, height: 100,
		borderRadius: 50,
		zIndex: 1, marginTop: -510
	},
	innerlabel: {
		marginTop: 10,
		fontSize: 18,
		color: Constants.black,
		fontFamily: "Montserrat-Bold"
	},
	logoSection: {
		width: 120,
		height: 115,
		position: "absolute",
		zIndex: 2,
		top: 0,
		backgroundColor: Constants.green,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 65,
		top: -60
	},
	logo: { width: 80, height: 50 },
	inputLogo: {
		width: 20,
		height: 20,
		resizeMode: 'contain',
		marginTop: 14,
		marginLeft: 15
	},
	forgotLine: {
		width: "auto", height: 30,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121',
		margin: 5
	},
	loginText: {
		fontSize: 22,
		color: Constants.black,
		fontFamily: "Montserrat-Bold"
	},
	input: {
		marginLeft: 10,
		width: "80%",
	},
	inputSection: {
		borderWidth: 1,
		borderColor: 'lightgrey',
		borderRadius: 25,
		width: '85%',
		height: 50,
		flexDirection: "row",
		marginTop: 10,
		marginBottom: 5
	},
	loginButton: {
		borderWidth: 1,
		minWidth: '85%',
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5,
		borderRadius: 25,
		backgroundColor: Constants.green,
		borderColor: Constants.green
	},
	socialSection: {
		width: "auto",
		height: 70,
		alignContent: "center",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	socialButton: {
		height: 55,
		width: 55,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 2,
		borderRadius: 50,
		marginRight: 5
	},
	signUpBar: {
		width: "80%",
		height: 40,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	signUpLline: {
		height: 1,
		backgroundColor: "lightgrey",
		flex: 1
	}
});