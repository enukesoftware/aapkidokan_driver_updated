import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Dimensions, Image, ScrollView, Switch, Platform } from 'react-native';
import Images from '../../../Componants/Images';
import Constants from '../../../Componants/Constants';
import SInfo from 'react-native-sensitive-info';
import Fonts from '../../../Componants/Fonts';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import NavigationService from '../../../Componants/NavigationService';
import Spinner from 'react-native-loading-spinner-overlay';
import * as ApiManager from './../../../ApiManager/ApiManager';
import { connect } from 'react-redux';
import * as Actions from '../../../Redux/Action/Index';

class MoreScreen extends Component {
	// static navigationOptions = {
	//     title: 'Home',
	//     // header:null //hide the header
	// };
	constructor(props) {
		super(props);
		this.state = {
			screenWidth: Dimensions.get("window").width,
			products: ['Order History', 'My Profile', 'Logout'],
			is_online: Constants.is_available,
			loading: false,
		};
	}

	static navigationOptions = ({ navigation }) => ({
		header: null
	});

	logOut = async () => {
		const netInfo = await NetInfo.fetch();
		if (!netInfo.isConnected) {
			Toast.showWithGravity(
				"Internet is not available",
				Toast.LONG,
				Toast.BOTTOM //TOP,BOTTOM
			);
			return
		}
		this.setState({ loading: true });
		ApiManager.logOut().then((res) => {
			setTimeout(() => {
				this.setState({ loading: false });
			}, 100);
			if (res && res.success && res.code == 200) {
				setTimeout(async () => {
					await SInfo.deleteItem('token', Constants.sharedPreference);
					await SInfo.deleteItem('userdetail', Constants.sharedPreference);
					await SInfo.deleteItem('is_online', Constants.sharedPreference);
					await SInfo.deleteItem('notifications', Constants.sharedPreference);
					Constants.user = {};
					NavigationService.navigate("Login");
					this.props.saveDriverData();
				}, 500);
			}
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => {
					console.log(JSON.stringify(err))
				});
			}, 100);
		})
	}

	updateStatus = async () => {
		this.setState({ loading: true });
		ApiManager.updateStatus(this.state.is_online).then((res) => {
			setTimeout(() => {
				this.setState({ loading: false });
			}, 100);
			if (res && res.success && res.code == 200) {
				setTimeout(async () => {
					await SInfo.setItem('is_online', this.state.is_online ? JSON.stringify(this.state.is_online) : '', Constants.sharedPreference);
					Constants.is_available = this.state.is_online;
					// NavigationService.navigate("Login");
				}, 500);
			} else this.setState({ is_online: Constants.is_available })
		}).catch((err) => {
			setTimeout(() => {
				this.setState({ loading: false, is_online: Constants.is_available }, () => {
					console.log(JSON.stringify(err))
				});
			}, 100);
		})
	}

	renderList = () => {
		return (
			<View style={{
				flex: 1,
				justifyContent: "flex-start",
			}}>
				<ScrollView style={{ flex: 1 }}>
					<TouchableOpacity style={{
						height: 50,
						width: this.state.screenWidth,
						flexDirection: "row",
						justifyContent: "flex-start",
						alignItems: "center",
						borderWidth: 2,
						borderColor: Constants.backgroundPageColor
					}} onPress={() => NavigationService.navigate("OrderHistory")}>
						<View style={{
							height: 50, width: 50,
							justifyContent: "center",
							alignItems: "center",
						}} >
							<Image style={{
								width: 25,
								height: 25,
							}} resizeMode="cover" source={Images.ImgOrders} />
						</View>
						<Text style={{
							fontFamily: Fonts.bold, color: Constants.black,
							fontSize: 16,
						}}>Order History</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() => NavigationService.navigate("MyProfile")}
						style={{
							height: 50,
							width: this.state.screenWidth,
							flexDirection: "row",
							justifyContent: "flex-start",
							alignItems: "center",
							borderWidth: 2,
							borderColor: Constants.backgroundPageColor
						}}>
						<View style={{
							height: 50, width: 50,
							justifyContent: "center",
							alignItems: "center",
						}} >
							<Image style={{
								width: 25,
								height: 25,
							}} resizeMode="cover" source={Images.ImgUserCircle} />
						</View>
						<Text style={{
							fontFamily: Fonts.bold, color: Constants.black,
							fontSize: 16,
						}}>My Profile</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						height: 50,
						width: this.state.screenWidth,
						flexDirection: "row",
						justifyContent: "flex-start",
						alignItems: "center",
						borderWidth: 2,
						borderColor: Constants.backgroundPageColor
					}} onPress={() => NavigationService.navigate("Notifications")}>
						<View style={{
							height: 50, width: 50,
							justifyContent: "center",
							alignItems: "center",
						}} >
							<Image style={{
								width: 25,
								height: 25,
							}} resizeMode="cover" source={Images.ImgBell} />
						</View>
						<Text style={{
							fontFamily: Fonts.bold, color: Constants.black,
							fontSize: 16,
						}}>Notifications</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						height: 50,
						width: this.state.screenWidth,
						flexDirection: "row",
						justifyContent: "flex-start",
						alignItems: "center",
						borderWidth: 2,
						borderColor: Constants.backgroundPageColor
					}} onPress={() => this.logOut()}>
						<View style={{
							height: 50, width: 50,
							justifyContent: "center",
							alignItems: "center",
						}} >
							<Image style={{
								width: 25,
								height: 20,
							}} resizeMode="cover" source={Images.ImgLogout} />
						</View>
						<Text style={{
							fontFamily: Fonts.bold, color: Constants.black,
							fontSize: 16,
						}}>Logout</Text>
					</TouchableOpacity>
					<View style={{
						height: 50,
						width: this.state.screenWidth,
						justifyContent: "center",
						alignItems: "center",
						borderColor: Constants.backgroundPageColor
					}}>
						<Text style={{
							fontFamily: Fonts.bold, color: Constants.black,
							fontSize: 16,
						}}>Version {(Platform.OS === "android") ? Constants.versionAndroid : Constants.versionIos}</Text>
					</View>
				</ScrollView>
			</View>
		)
	}

	render() {
		console.log("More page")
		return (
			<View style={{ flex: 1, backgroundColor: Constants.backgroundScreenColor }}>
				<Spinner size="large"
					visible={this.state.loading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<View style={{ width: '100%', height: 65, backgroundColor: Constants.green, alignItems: 'center' }}>
					<Text style={{ fontFamily: Fonts.bold, color: 'white', fontSize: 18, marginTop: 35 }}>More</Text>
				</View>
				<View style={{ width: '100%', height: 350, backgroundColor: "white" }}>
					{this.renderList()}
					<View style={{
						width: '100%', height: 100,
						alignItems: 'center', justifyContent: "center"
					}}>
						<Text style={{ fontFamily: Fonts.bold, color: Constants.black, fontSize: 16, marginBottom: 10 }}>Change Your Availability Status</Text>
						<Switch
							onValueChange={is_online => {
								this.setState({ is_online }, () => {
									this.updateStatus();
								})
							}}
							value={this.state.is_online}
						/>
					</View>
				</View>
			</View>
		)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		saveDriverData: () => dispatch(Actions.saveDriverData({})),
	}
}

export default connect(null, mapDispatchToProps)(MoreScreen)