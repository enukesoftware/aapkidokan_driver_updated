import React, { Component } from 'react';
import { View, Text, TextInput, Image, Dimensions, StyleSheet, TouchableOpacity, AsyncStorage, Alert, Animated, StatusBar, TouchableHighlight, Platform, ActivityIndicator } from 'react-native';
import Images from '../../../Componants/Images';
import Constants from '../../../Componants/Constants';
import Spinner from 'react-native-loading-spinner-overlay';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import ImagePicker from "react-native-image-picker";
import SInfo from 'react-native-sensitive-info';
import Toast from 'react-native-simple-toast';
import Fonts from '../../../Componants/Fonts';
import NetInfo from "@react-native-community/netinfo";
import NavigationService from '../../../Componants/NavigationService';

export default class ProfileScreen extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			user: Constants.user,
			mobile: '',
			loading: false,
			editable: false,
			contact_number: Constants.user.contact_number,
			full_name: Constants.user.full_name,
			imageUpdated: false
		};
	}

	componentDidMount = async () => {
		// SocialHelper._configureGoogleSignIn();
		const netInfo = await NetInfo.fetch();
		if (!netInfo.isConnected) {
			Toast.showWithGravity("Internet is not available", Toast.LONG, Toast.BOTTOM);
			return
		}
		let mobile = "";
		if (Constants.user.facebook_id) {
			mobile = "NA";
		} else {
			// mobile = "97******87";
			mobile = Constants.user.contact_number.slice(0, 2) + "******" + Constants.user.contact_number.slice(8, 10);
		}
		if (!Constants.user.email) Constants.user.email = "NA";
		this.setState({ user: Constants.user, mobile: mobile }, () => {
			console.log(this.state.user);
			console.log(Constants.user);
		});
	}

	showImagePicker() {
		// this.setState({ loading: true });
		const options = {
			rotation: 360,
			allowsEditing: true,
			noData: true,
			mediaType: "photo",
			maxWidth: 300,
			maxHeight: 300,
			storageOptions: {
				skipBackup: true
			}
		}
		ImagePicker.showImagePicker(options, response => {
			console.log("Response = ", response);
			// this.setState({ loading: false });
			if (response.didCancel) {
				//   console.log(JSON.stringify(source));
				console.warn('User cancelled image picker');
			} else if (response.error) {
				//  console.log(JSON.stringify(source));
				console.warn('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				//  console.log('User tapped custom button: ', response.customButton);
			} else {
				delete response.data;
				let user = Constants.user;
				user.picture = response.uri;
				const source = { uri: response.uri };
				this.setState({ user: user, imageUpdated: true }, () => {
					console.log("Image captured");
				});
				console.warn(source);
			}
		});
	}

	update = async () => {
		if (!this.state.full_name || !this.state.full_name.length) {
			Toast.showWithGravity("Please enter your full name", Toast.SHORT, Toast.BOTTOM);
			return false
		}
		if (!this.state.contact_number) {
			Toast.showWithGravity('Please enter contact number', Toast.LONG, Toast.CENTER);
			return
		}
		if (isNaN(this.state.contact_number) || (this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 11)) || (!this.state.contact_number.startsWith('0') && (this.state.contact_number.length != 10))) {
			Toast.showWithGravity('Contact number is not valid', Toast.LONG, Toast.CENTER);
			return
		}
		const netInfo = await NetInfo.fetch();
		if (!netInfo.isConnected) {
			Toast.showWithGravity("Internet is not available", Toast.LONG, Toast.BOTTOM);
			return
		}
		this.setState({ loading: true });
		let data = new FormData();
		if (this.state.imageUpdated) {
			data.append('picture', {
				uri: Constants.user.picture,
				type: "image/jpeg", // or photo.type
				name: "profile_picture"
			});
		}
		data.append('contact_number', this.state.contact_number);
		data.append('full_name', this.state.full_name);
		let url = Constants.url + 'user/profile/' + Constants.user._id;
		let token = await SInfo.getItem('token', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
		const header = {
			"Content-Type": "multipart/form-data",
			"Authorization": "Bearer " + token
		};
		fetch(url, {
			method: 'PUT',
			headers: header,
			body: data
		}).then((res) => { return res.json() })
			.then(async (res) => {
				setTimeout(() => {
					this.setState({ loading: false });
				}, 100);
				if (res.code && res.code.code == 401) {
					this.props.navigation.navigate("Login");
				} else if (res && res.success && res.code == 200) {
					if (res.data.driver.picture && !res.data.driver.picture.startsWith("http")) {
						res.data.driver.picture = Constants.imageBaseURL + res.data.driver.picture;
					}
					await SInfo.setItem('userdetail', JSON.stringify(res.data.driver), { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
					Constants.user = res.data.driver;
					let mobile = res.data.driver.contact_number.slice(0, 2) + "******" + res.data.driver.contact_number.slice(8, 10);
					this.setState({
						user: res.data.driver,
						contact_number: res.data.driver.contact_number,
						full_name: res.data.driver.full_name,
						mobile: mobile,
						imageUpdated: false,
						editable: false
					}, () => {
						Toast.showWithGravity("Profile updated successfully ...", Toast.SHORT, Toast.BOTTOM);
						console.log("Updated");
						console.log(JSON.stringify(this.state));
					});
				} else {
					setTimeout(() => {
						Toast.showWithGravity(
							res.singleStringMessage ? res.singleStringMessage : "Error " + res.error + " genereted",
							Toast.SHORT, Toast.BOTTOM
						);
						// Alert.alert(JSON.stringify(res.singleStringMessage));
					}, 110)
				}
			});
	}

	renderEditPage = () => {
		return (
			<View style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: Constants.backgroundPageColor,
				marginTop: 40,
			}}>
				<View style={styles.inputSection}>
					<Image source={Images.ImgUserIcon} style={styles.inputLogo} />
					<TextInput
						style={styles.input}
						value={this.state.full_name}
						placeholder='Enter your full name'
						placeholderTextColor={Constants.placeHolderColor}
						onChangeText={(text) => { this.setState({ full_name: text }) }}
					/>
				</View>
				<View style={styles.inputSection}>
					<Image source={Images.ImgMobile} style={styles.inputLogo} />
					<TextInput
						style={styles.input}
						value={this.state.contact_number}
						maxLength={11}
						placeholder='Eenter Mobile Number'
						placeholderTextColor={Constants.placeHolderColor}
						onChangeText={(text) => { this.setState({ contact_number: text }) }}
						onSubmitEditing={() => this.update()}
					/>
				</View>
				<TouchableOpacity style={{
					backgroundColor: Constants.green,
					padding: 10,
					paddingLeft: 17,
					paddingRight: 17,
					borderRadius: 25,
					marginTop: 50
				}} onPress={() => this.update()}>
					<Text style={{ color: "white", fontSize: 16, fontFamily: Fonts.bold }}>UPDATE</Text>
				</TouchableOpacity>
			</View>
		)
	}

	renderProfilePage = () => {
		return (
			<View style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: Constants.backgroundPageColor
			}}>
				<View style={{
					width: "100%",
					minHeight: 90,
					flexDirection: "row"
				}}>
					<View style={{
						width: "30%",
						alignItems: "center",
						justifyContent: "space-around"
					}}>
						<Image source={Images.ImgMobile} style={{ width: 15, height: 25 }}></Image>
						<Text style={{ color: Constants.black, fontSize: 14, fontFamily: Fonts.medium }}>{this.state.mobile}</Text>
					</View>
					<View style={{
						width: "40%",
						alignItems: "center",
						justifyContent: "space-around",
						borderLeftWidth: 1,
						borderRightWidth: 1,
						borderColor: "darkgray",
						paddingLeft: 5,
						paddingRight: 5,
					}}>
						<Image source={Images.ImgUserIcon} style={{ width: 25, height: 25 }} resizeMode="contain"></Image>
						<Text style={{ color: Constants.black, fontSize: 14, textAlign: "center", fontFamily: Fonts.medium }}>{this.state.user.email}</Text>
					</View>
					<View style={{
						width: "30%",
						alignItems: "center",
						justifyContent: "space-around"
					}}>
						<Image source={Images.ImgPassword} style={{ width: 18, height: 22 }}></Image>
						<Text style={{ color: Constants.black, fontSize: 14, fontFamily: Fonts.medium }}>******</Text>
					</View>
				</View>
				<TouchableOpacity style={{
					backgroundColor: Constants.green,
					padding: 10,
					paddingLeft: 17,
					paddingRight: 17,
					borderRadius: 25,
					marginTop: 50
				}} onPress={() => NavigationService.navigate("ChangePassword")}>
					<Text style={{ color: "white", fontSize: 16, fontFamily: Fonts.bold }}>CHANGE PASSWORD</Text>
				</TouchableOpacity>
			</View>
		)
	}

	render() {
		const Screen =
		{
			heignt: Dimensions.get('window').height,
			width: Dimensions.get('window').width,
			scale: Dimensions.get('window').scale,
			fontScale: Dimensions.get('window').fontScale,
		};
		let animatBackgroundColor = this.state.scrollY.interpolate({
			inputRange: [0, Screen.heignt / 3],
			outputRange: ['transparent', '#1faf4b'],
			extrapolate: 'clamp'
		});
		let translateY = this.state.scrollY.interpolate({
			inputRange: [0, Screen.heignt / 3],
			outputRange: [0, -((Screen.heignt / 3) - (getStatusBarHeight(false) + 44))],
			extrapolate: 'clamp',
		});

		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner
					size="large"
					visible={this.state.loading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					contentContainerStyle={{ paddingTop: Screen.heignt / 3 }}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{
						this.state.editable ?
							this.renderEditPage() :
							this.renderProfilePage()
					}
				</Animated.ScrollView>

				<Animated.Image style={{
					position: 'absolute',
					top: 0,
					left: 0,
					right: 0,
					height: Screen.heignt / 3.5,
					width: '100%',
					backgroundColor: 'white',
					alignItems: 'center',
					justifyContent: 'flex-end',
					transform: [{ translateY }],
					backgroundColor: '#f5f5f5',
					zIndex: -1
				}}
					resizeMode='stretch'
					source={Images.ImgHeader}
				/>
				<Animated.View style={{
					position: 'absolute',
					top: 0,
					left: 0,
					right: 0,
					height: Screen.heignt / 2.2,
					backgroundColor: 'transparent',
					zIndex: 0,
					alignItems: 'center',
					justifyContent: 'center',
					transform: [{ translateY }],
				}}>
					<View style={{ alignItems: 'center', justifyContent: 'center' }}>
						<TouchableOpacity style={{
							width: 120,
							height: 120,
						}} onPress={() => {
							if (this.state.editable) {
								this.showImagePicker();
							}
						}}>
							{
								this.state.user.picture ?
									<View style={{
										width: "100%",
										height: "100%",
										resizeMode: 'cover',
										marginTop: -20,
										borderRadius: 60,
										position: "absolute",
										zIndex: -1,
										justifyContent: "center",
										alignItems: "center"
									}} >
										<ActivityIndicator size="large" color={Constants.lightgreen} />
									</View> : null
							}

							{
								this.state.user.picture ?
									<Image style={{
										width: "100%",
										height: "100%",
										resizeMode: 'cover',
										marginTop: -20,
										borderRadius: 60,
										position: "absolute",
										zIndex: 1
									}} source={{ uri: this.state.user.picture }} /> :
									<Image style={{
										width: 120,
										height: 120,
										resizeMode: 'cover',
										marginTop: -20,
										borderRadius: 60,
										position: "absolute",
										zIndex: 1
									}} source={Images.ImgDefaultUser} />
							}
						</TouchableOpacity>
						<View style={{
							width: Screen.width,
							height: 45,
							alignItems: "center"
						}}>
							<Text style={{
								color: Constants.black, fontFamily: Fonts.semiBold,
								fontSize: 18, marginTop: 10, textTransform: "capitalize"
							}}>{this.state.user.full_name}</Text>
						</View>
					</View>
				</Animated.View>
				<Animated.View style={{ top: 0, left: 0, right: 0, position: 'absolute', flexDirection: 'row', backgroundColor: animatBackgroundColor, ...Platform.select({ ios: { height: (getStatusBarHeight(true) + 40), }, android: { height: (getStatusBarHeight(true) + 40), } }) }}>
					<Animated.View style={{
						height: '100%',
						width: 60, marginLeft: 10,
						justifyContent: 'center', alignItems: 'center', flexDirection: 'row'
					}}>
						<TouchableHighlight style={{ height: 35, width: 35, ...Platform.select({ ios: { marginTop: 45 }, android: { marginTop: 14 } }) }} underlayColor='transparent'
							onPress={() => {
								this.props.navigation.goBack()
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.ImgBack} />
						</TouchableHighlight>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						marginLeft: 5,
						...Platform.select({
							ios: { marginTop: 15 }, android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						width: this.state.screenWidth - 120
					}}>
						<Text style={{
							fontSize: 16, color: 'white',
							fontFamily: Fonts.bold, width: 100
						}}>{this.state.editable ? "Edit" : "My"} Profile</Text>
					</Animated.View>
					<Animated.View style={{
						height: '100%', width: 60,
						justifyContent: 'center', alignItems: 'center', flexDirection: 'row'
					}}>
						<TouchableHighlight style={{
							height: 35, width: 35,
							...Platform.select({
								ios: { marginTop: 40 },
								android: { marginTop: 10 }
							})
						}} underlayColor='transparent' onPress={() => {
							this.setState({ editable: !this.state.editable })
						}}>
							{
								this.state.editable ?
									<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.ImgCloseWhite} /> :
									<Image style={{ height: 20, width: 20 }} resizeMode='contain' source={Images.ImgEditWhite} />
							}
						</TouchableHighlight>
					</Animated.View>
				</Animated.View>
				<StatusBar backgroundColor={Constants.green} barStyle="light-content" />
			</View>
		)
	}
}

const styles = StyleSheet.create({
	bgImage: {
		width: "100%",
		height: "100%",
		position: "absolute"
	},
	outerSection: {
		flex: 1,
		// height: (Dimensions.get("window").height),
		alignItems: 'center',
		justifyContent: 'center',
		position: "relative"
	},
	innerSection: {
		backgroundColor: 'white',
		alignItems: 'center',
		shadowOpacity: 1,
		shadowRadius: 3,
		shadowOffset: { height: 0, width: 0 },
		elevation: 1, zIndex: 0, borderRadius: 10,
		width: ((Dimensions.get("window").width) - 50),
		minHeight: 480,
		position: "relative",
		marginTop: 45,
		paddingTop: 50,
		marginBottom: 0
	},
	innerImage: {
		width: 100, height: 100,
		borderRadius: 50,
		zIndex: 1, marginTop: -510
	},
	innerlabel: {
		marginTop: 10,
		fontSize: 18,
		color: Constants.black,
		fontFamily: "Montserrat-Bold"
	},
	logoSection: {
		width: 120,
		height: 115,
		position: "absolute",
		zIndex: 2,
		top: 0,
		backgroundColor: Constants.green,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 65,
		top: -60
	},
	logo: { width: 80, height: 50 },
	inputLogo: {
		width: 20,
		height: 20,
		resizeMode: 'contain',
		marginTop: 14,
		marginLeft: 15
	},
	forgotLine: {
		width: "auto", height: 30,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121',
		margin: 5
	},
	loginText: {
		fontSize: 22,
		color: Constants.black,
		fontFamily: "Montserrat-Bold"
	},
	input: {
		marginLeft: 10,
		width: "80%",
	},
	inputSection: {
		borderWidth: 1,
		borderColor: 'lightgrey',
		borderRadius: 25,
		width: '85%',
		height: 50,
		flexDirection: "row",
		marginTop: 10,
		marginBottom: 5
	},
	loginButton: {
		borderWidth: 1,
		minWidth: '85%',
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5,
		borderRadius: 25,
		backgroundColor: Constants.green,
		borderColor: Constants.green
	},
	socialSection: {
		width: "auto",
		height: 70,
		alignContent: "center",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	socialButton: {
		height: 55,
		width: 55,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 2,
		borderRadius: 50,
		marginRight: 5
	},
	signUpBar: {
		width: "80%",
		height: 40,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121'
	},
	signUpLline: {
		height: 1,
		backgroundColor: "lightgrey",
		flex: 1
	}
});