import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity, Animated, Alert, Platform, TextInput,DeviceEventEmitter } from 'react-native';
import Images from '../../../Componants/Images';
import Constants from '../../../Componants/Constants';
import Spinner from 'react-native-loading-spinner-overlay';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import Fonts from '../../../Componants/Fonts';
import SInfo from 'react-native-sensitive-info';
import moment from 'moment';
import { connect } from 'react-redux';
import { getNotificationApi, deleteNotificationsApi } from '../../../ApiManager/ApiManager';


class Notifications extends Component {
	static navigationOptions = {
		// title: 'Home',
		header: null //hide the header
	};

	constructor(props) {
		super(props);
		this.state = {
			scrollY: new Animated.Value(0.01),
			screenWidth: Dimensions.get("window").width,
			screenHeight: Dimensions.get("window").height,
			user: Constants.user,
			list: [],
			headerHeight: 0,
			dateTime: '2020-03-02T05:43:43.612Z',
			loading: false
		};

		// this.addNotificationHandler = this.addNotificationHandler.bind(this);
	}

	componentDidMount() {
		// this.getNotifications()
		this.setState({ loading: true })
		this.callNotificationApi();
	}

	deleteNotifications = async () => {
		console.log("DELETE:")
		await SInfo.deleteItem('notifications', Constants.sharedPreference);
		this.setState({
			list: []
		})
	}
	callDeleteNotificationApi() {
		this.setState({ loading: true })
		deleteNotificationsApi().then(res => {
			if (res && res.success) {
				this.setState({ loading: false, list: [] })
			} else {
				this.setState({ loading: false })
			}
		}).catch(e => {
			this.setState({ loading: false })
		})
	}
	callNotificationApi() {
		const data = { offset: this.state.offset, limit: this.state.limit }
		getNotificationApi(data).then((res) => {
			console.log(res)
			if (res && res.data && res.data.rows && res.data.rows.length > 0) {
				let list = []
				let sortedObj = res.data.rows
				sortedObj.reverse()
				sortedObj.forEach(element => {
					list.push(element.data)
				});
				this.setState({
					list
				}, () => {
					this.setState({ loading: false })
				})
			} else {
				this.setState({ loading: false })
			}
		}).catch((e) => {
			this.setState({ loading: false })
		})
	}

	getNotifications = async () => {
		// await SInfo.deleteItem('notifications', Constants.sharedPreference);
		let notifications = await SInfo.getItem('notifications', Constants.sharedPreference);
		if (notifications) {
			let parsedData = JSON.parse(notifications)
			let sortedObj = parsedData.sort(function (a, b) {
				return new Date(b.createdAt) - new Date(a.createdAt);
			});
			this.setState({
				list: sortedObj
			}, () => {
				console.log("NOTI_4:", JSON.stringify(this.state.list))
			})
			// list = parsedData
			console.log("NOTI_3:" + JSON.stringify(parsedData))
		}

	}

	renderPage = () => {
		return (
			<View style={{
				flex: 1,
				marginTop: this.state.headerHeight - 18,
				// height: (this.state.screenHeight - this.state.headerHeight),
				alignItems: 'center',
				justifyContent: 'flex-start',
				backgroundColor: "red",
				backgroundColor: Constants.backgroundPageColor,
				marginBottom: 10
			}}>
				{
					this.state.list.map((item, index) => (
						<View key={index} style={{
							width: this.state.screenWidth - 20,
							minHeight: 70,
							borderBottomWidth: this.state.list[index + 1] ? 2 : null,
							borderBottomColor: "lightgrey",
							justifyContent: "center",
							paddingBottom: 10,
							paddingTop: 10,
						}}>
							<View style={{
								width: "100%",
								flexDirection: "row",
								justifyContent: "space-between"
							}}>
								<Text style={{
									fontSize: 15,
									color: Constants.black, fontWeight: '600',
									width: this.state.screenWidth - 90,
								}}>{item.notificationBody ? item.notificationBody : "NA"}</Text>
								{
									item.isNew ?
										<View style={{
											backgroundColor: "#f0cd98",
											height: 30,
											paddingLeft: 15,
											paddingRight: 15,
											justifyContent: "center",
											borderRadius: 20
										}}>
											<Text style={{
												fontSize: 15,
												color: Constants.black, fontWeight: '600'
											}}>NEW</Text>
										</View>
										: null
								}

							</View>
							<Text style={{
								fontSize: 12, marginTop: 5,
								color: Constants.black, fontWeight: '400'
							}}>{item.createdAt ? moment(item.createdAt).format("hh:mm A DD-MMM-YY") : "NA"}</Text>
						</View>
					))
				}
			</View>
		)
	}

	render() {
		console.log("More page");
		return (
			<View style={{
				flex: 1,
				paddingTop: getStatusBarHeight(false),
				backgroundColor: '#f5f5f5',
			}}>
				<Spinner
					size="large"
					visible={this.state.loading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<Animated.ScrollView
					scrollEventThrottle={20}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.state.scrollY } } },
					])}
					collapsable={true}
					bounces={false}
					style={{ flex: 1 }}>
					{this.renderPage()}
				</Animated.ScrollView>
				<Animated.View style={{
					top: 0, left: 0,
					right: 0, position: 'absolute',
					flexDirection: 'row',
					...Platform.select({
						ios: { height: (getStatusBarHeight(true) + 40), },
						android: {
							height: (getStatusBarHeight(true) + 40),
						}
					}),
					backgroundColor: '#1faf4b',
					// backgroundColor: 'blue',
					paddingBottom: 10
				}} onLayout={(event) => {
					this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
						console.log("Header height is:--" + this.state.headerHeight);
					})
					console.log(event.nativeEvent.layout);
				}}>
					<Animated.View style={{
						height: '100%', width: 60,
						minWidth: 55,
						minHeight: 55,
						justifyContent: 'center',
						alignItems: 'center',
						position: "absolute",
						left: 0,
						top: 0,
						zIndex: 2,
						paddingLeft: 15
					}}>
						<TouchableOpacity style={{
							height: 45, width: 45,
							...Platform.select({
								ios: { marginTop: 50 },
								android: { marginTop: 19 }
							}),
							// backgroundColor: "red"
						}} underlayColor='transparent'
							onPress={() => {
								this.props.navigation.goBack()
							}}>
							<Image style={{ height: 20, width: 30 }} resizeMode='contain' source={Images.ImgBack} />
						</TouchableOpacity>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						marginLeft: 5,
						...Platform.select({
							ios: { marginTop: 15 },
							android: { marginTop: 0 }
						}),
						justifyContent: 'center',
						// alignItems: "center",
						width: '100%',
						// backgroundColor: 'blue',
					}} onLayout={(event) => {
						this.setState({ headerHeight: event.nativeEvent.layout.height }, () => {
							console.log("Header height is:--" + this.state.headerHeight);
						})
						console.log(event.nativeEvent.layout);
					}}>
						<Text style={{
							fontSize: 16, color: 'white',
							fontWeight: '500', width: 137,
							marginLeft: 60
						}}>Notifications</Text>
					</Animated.View>
					<Animated.View style={{
						height: '100%',
						width: 80,
						minHeight: 55,
						justifyContent: 'center',
						alignItems: 'flex-start',
						position: "absolute",
						right: 10,
						top: 0,
						zIndex: 2,
						paddingLeft: 15,
						// backgroundColor: "yellow"
					}}>
						<TouchableOpacity style={{
							height: 45,
							...Platform.select({
								ios: { marginTop: 50 },
								android: { marginTop: 19 }
							}),
							// backgroundColor: "red"
						}} underlayColor='transparent'
							onPress={() => {
								// this.props.navigation.goBack()
								// this.deleteNotifications()
								this.callDeleteNotificationApi()
							}}>
							<Text style={{ fontSize: 14, color: 'white', fontWeight: '500' }}>Delete All</Text>
						</TouchableOpacity>
					</Animated.View>
				</Animated.View>
			</View>
		)
	}
}


const mapStateToProps = (state) => {
	console.log('Notifications:', state.getNotifications)
	return ({
		getNotifications: state.getNotifications
	})
}

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications)
