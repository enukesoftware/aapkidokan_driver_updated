import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import Constants from '../../../Componants/Constants';
import CardView from 'react-native-cardview';
import moment from 'moment';
import Fonts from '../../../Componants/Fonts';

export default class OrderDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			order: {},
			products: [],
		};
	}

	static navigationOptions = {
		title: 'View Detail',
		headerStyle: {
			backgroundColor: Constants.green,
			alignItems: "flex-start",
			textAlign: "left"
		},
		headerTintColor: '#fff',
		headerTitleStyle: {
			fontFamily: Fonts.bold,
			fontSize: 18,
			position: "absolute",
			left: -10
			// marginBottom: 10
		},
	};

	componentWillMount() {
		console.log(this.props.navigation.state.params.order)
		let paramsData = this.props.navigation.state.params.order
		this.setState({
			products: paramsData.products,
			order: paramsData
		})
	}

	render() {
		let { products } = this.state;
		return (
			<View style={{
				flex: 1,
				justifyContent: "flex-start",
			}}>
				<ScrollView style={{ flex: 1 }}>
					<CardView cardElevation={5} cardMaxElevation={5} cornerRadius={5} style={styles.productBox}>
						<View style={{
							backgroundColor: Constants.lightgreen,
							width: "100%",
							height: 30,
							alignItems: "center",
							flexDirection: "row",
							paddingLeft: 10
						}}>
							<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Delivery Date : </Text>
							<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.bold }}>{moment(this.state.order.deliver_end_time).format("DD-MMM-YY HH:mm:ss")}</Text>
						</View>
						<View style={{
							height: 70, width: '100%',
							flexDirection: 'row', alignItems: "center",
							justifyContent: "space-between"
						}}>
							<Text style={{
								color: Constants.black,
								fontSize: 20,
								fontFamily: Fonts.bold,
								marginLeft: 12,
							}}>{this.state.order.address.delivery.full_name}</Text>
							<View style={{
								height: 40,
								backgroundColor: Constants.lightgreen,
								paddingLeft: 15,
								paddingRight: 5,
								justifyContent: "center",
								alignItems: "center",
								borderTopLeftRadius: 20,
								borderBottomLeftRadius: 20
							}}>
								<Text style={{ color: Constants.black, fontSize: 16, fontFamily: Fonts.semiBold }}>{(this.state.order.total_amount_after_tax + this.state.order.delivery_charges) - this.state.order.discount} {Constants.currency}</Text>
							</View>

						</View>
						<View style={{
							height: 50, width: '100%',
							flexDirection: 'row',
						}}>
							<View style={{ height: '100%', width: '33%', }}>
								<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Order ID</Text>
								<Text style={{ fontFamily: Fonts.bold, fontSize: 12, color: Constants.black, marginLeft: 14 }}>#{this.state.order.order_id}</Text>
							</View>
							<View style={{ backgroundColor: 'lightgray', height: 35, width: 1 }}>
							</View>
							<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
								<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Shop </Text>
								<Text style={{
									marginTop: 1, fontFamily: Fonts.bold,
									fontSize: 12, color: Constants.black, textAlign: "center"
								}}>{this.state.order.store.name}</Text>
							</View>
							<View style={{ backgroundColor: 'lightgray', height: 35, width: 1 }}>
							</View>
							<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
								<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Payment Status </Text>
								<Text style={{
									marginTop: 1,
									fontFamily: Fonts.bold, fontSize: 12,
									color: (this.state.order.payment_type == 2 || this.state.order.status == 3) ? Constants.green : Constants.gold,
									marginLeft: 14
								}}>{(this.state.order.payment_type == 2 || this.state.order.status == 3) ? "Paid" : "Pending"} </Text>
							</View>
						</View>
						<View style={{ backgroundColor: 'lightgray', height: 1, width: '100%' }}></View>
						<View style={{ flexDirection: 'row', width: '100 %', padding: 10 }}>
							<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Commission </Text>
							<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.bold }}>{(this.state.order.driver_commission?this.state.order.driver_commission:0) + " " + Constants.currency} </Text>
						</View>
					</CardView>
					<CardView cardElevation={5}
						cardMaxElevation={5}
						cornerRadius={5}
						style={styles.productBox}>
						{
							products.map((product, index) => (
								<View style={{
									height: 90, width: "100%",
									backgroundColor: "white",
									flexDirection: "row",
									justifyContent: "flex-start",
									alignItems: "center",
									borderTopWidth: index != 0 ? 3 : 0,
									borderTopColor: index != 0 ? Constants.backgroundPageColor : "transparent"
								}}>
									<View style={{
										width: "20%",
										height: "100%", alignItems: "center",
										justifyContent: "center",
									}}>
										<Image source={{ uri: Constants.imageBaseURL + product.pictures[0] }}
											style={{ height: 82, width: "90%", resizeMode: "contain" }}></Image>
									</View>
									<View style={{
										width: "60%", alignItems: "flex-start",
										justifyContent: "center",
										height: 90, paddingLeft: 5
									}}>
										<Text numberOfLines={2} style={{
											fontSize: 12,
											fontFamily: Fonts.medium, color: Constants.black
										}}>{product.name}</Text>
										<Text style={{
											color: Constants.black,
											fontSize: 13, fontFamily: Fonts.medium,
											textAlign: "left", marginBottom: 3, marginTop: 3
										}}>{product.size}</Text>
										<Text style={{
											color: Constants.black,
											fontSize: 13, fontFamily: Fonts.semiBold,
											textAlign: "left"
										}}>{Constants.currency} {product.price} * {product.count}</Text>
									</View>
									<View style={{
										width: "20%", alignItems: "flex-end",
										height: 90, padding: 10, paddingTop: 15
									}}>
										<Text style={{
											color: Constants.green,
											fontSize: 13, fontFamily: Fonts.semiBold,
											textAlign: "right"
										}}>{product.count * product.price} {Constants.currency}</Text>
									</View>
								</View>
							))
						}
					</CardView>
					<CardView cardElevation={5}
						cardMaxElevation={5}
						cornerRadius={5}
						style={styles.productBox}>
						<View style={{
							width: "100%",
							height: 40,
							backgroundColor: Constants.lightgreen,
							justifyContent: "center"
						}}>
							<Text style={{
								color: Constants.black,
								fontSize: 14, fontFamily: Fonts.semiBold,
								marginLeft: 10,
							}}>Payment Summary</Text>
						</View>
						<View style={{
							width: "100%",
							height: "auto",
							justifyContent: "space-around",
							alignItems: "center",
							borderTopWidth: 2,
							paddingTop: 5,
							paddingBottom: 5,
							borderTopColor: Constants.backgroundPageColor
						}}>
							<View style={{
								width: "100%",
								height: 35,
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center"
							}}>
								<Text style={{
									fontSize: 13, marginLeft: 10,
									fontFamily: Fonts.medium, color: Constants.black
								}}>Total Price (Inc. Tax)</Text>
								<Text style={{
									fontSize: 15,
									fontFamily: Fonts.bold,
									color: Constants.black,
									marginRight: 10
								}}>{this.state.order.total_amount} {Constants.currency}</Text>
							</View>
							{
								this.state.order.taxes.map((tax, index) => (
									<View style={{
										width: "100%",
										height: 35,
										flexDirection: "row",
										justifyContent: "space-between",
										alignItems: "center"
									}} key={index}>
										<Text style={{
											fontSize: 13, marginLeft: 10,
											fontFamily: Fonts.medium, color: Constants.black
										}}>{tax.name}</Text>
										<Text style={{
											fontSize: 15,
											fontFamily: Fonts.semiBold,
											color: Constants.black,
											marginRight: 10
										}}>{tax.value} {Constants.currency}</Text>
									</View>
								))
							}
							<View style={{
								width: "100%",
								height: 35,
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center"
							}} >
								<Text style={{
									fontSize: 13, marginLeft: 10,
									fontFamily: Fonts.medium, color: Constants.black
								}}>Delivery Charge</Text>
								{
									this.state.order.delivery_charges ?
										<Text style={{
											fontSize: 15,
											fontFamily: Fonts.semiBold,
											color: Constants.black,
											marginRight: 10
										}}>{this.state.order.delivery_charges} {Constants.currency}</Text> :
										<Text style={{
											fontSize: 15,
											fontFamily: Fonts.bold,
											color: Constants.black,
											marginRight: 10
										}}>FREE</Text>
								}
							</View>
							{
								this.state.order.coupon ?
									<View style={{
										width: "100%",
										height: 35,
										flexDirection: "row",
										justifyContent: "space-between",
										alignItems: "center",
										borderTopWidth: 2,
										borderTopColor: Constants.backgroundPageColor
									}}>
										<Text style={{
											fontSize: 15,
											fontFamily: Fonts.bold,
											color: Constants.green,
											marginLeft: 10
										}}>{this.state.order.coupon.code}
											<Text style={{
												fontSize: 15,
												fontWeight: "500",
												color: Constants.black,
												marginLeft: 10
											}}> Applied</Text>
										</Text>
										<Text style={{
											fontSize: 15,
											fontFamily: Fonts.semiBold,
											color: Constants.black,
											marginRight: 10
										}}>- {this.state.order.discount} {Constants.currency}</Text>
									</View> : null
							}
						</View>
						<View style={{
							width: "100%",
							height: 50,
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center",
							borderTopWidth: 2,
							borderTopColor: Constants.backgroundPageColor
						}}>
							<Text style={{
								fontSize: 15,
								fontFamily: Fonts.bold,
								color: Constants.black,
								marginLeft: 10
							}}>Grand Total</Text>
							<Text style={{
								fontSize: 15,
								fontFamily: Fonts.bold,
								color: Constants.black,
								marginRight: 10
							}}>{(this.state.order.total_amount_after_tax + this.state.order.delivery_charges) - this.state.order.discount} {Constants.currency}</Text>
						</View>
					</CardView>
				</ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	bars: {
		height: 40,
		width: "100%",
		justifyContent: "center",
		paddingLeft: 15, marginTop: 10
	},
	Header: {
		width: '100%',
		height: 200,
		alignItems: 'center'
	},
	productBox: {
		marginTop: 10,
		backgroundColor: "white",
		height: "auto",
		width: '95%',
		borderRadius: 10,
		borderBottomWidth: 3,
		borderColor: Constants.backgroundPageColor,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: '2.5%',
		marginBottom: 10
	},
	headerlabel: {
		marginTop: 50, fontSize: 16,
		color: Constants.white, fontFamily: Fonts.bold
	}
});