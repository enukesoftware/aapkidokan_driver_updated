import React, { Component } from 'react';
import { View, TouchableOpacity, ActivityIndicator, Text, StyleSheet, Image, FlatList } from 'react-native';
import Images from '../../../Componants/Images';
import Constants from '../../../Componants/Constants';
import CardView from 'react-native-cardview';
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import * as ApiManager from './../../../ApiManager/ApiManager';
import Fonts from '../../../Componants/Fonts';
import Toast from 'react-native-simple-toast';
import NavigationService from '../../../Componants/NavigationService';

export default class OrderHistory extends Component {
	constructor(props) {
		super(props);
		var d = new Date();
		d.setDate(d.getDate() - 30)
		this.state = {
			orders: [],
			first_date: d,
			last_date: new Date(),
			isFirstDateOpened: false,
			isLastDateOpened: false,
			loading: false,
			error: "",
			pageNo: 1,
			isLoading: false,
			isRefreshing: false,
			isListEnd: false,
			loadingMore: false,
			totalCommission:0,
		};
	}

	componentWillMount = () => {
		this.getOrderHistory()
	}

	static navigationOptions = {
		title: 'Order History',
		headerStyle: {
			backgroundColor: Constants.green,
		},
		headerBackTitle: 'Back',
		headerTintColor: '#fff',
		headerTitleStyle: {
			fontFamily: Fonts.bold,
			fontSize: 18,
			position: "absolute",
			left: -10
			// marginBottom: 10
		},
	};

	showFirstDate = () => {
		this.setState({ isFirstDateOpened: true });
	};

	showLastDate = () => {
		this.setState({ isLastDateOpened: true });
	};

	getFirstDate = (date) => {
		this.setState({ first_date: date }, () => {
			console.log("First date has been picked: ", date);
		});
		this.hideDateTimePicker();
	}

	getLastDate = (date) => {
		this.setState({ last_date: date }, () => {
			console.log("Last date has been picked: ", date);
		});
		this.hideDateTimePicker();
	}

	getOrderHistory = () => {
		let { pageNo, orders,totalCommission } = this.state;
		let body = {
			from_date: moment(this.state.first_date).format("DD-MMM-YY"),
			to_date: moment(this.state.last_date).format("DD-MMM-YY")
		};
		console.log(body)
		this.setState({ isLoading: true }, () => {
			ApiManager.homeData(body, pageNo).then((res) => {
				setTimeout(() => {
					this.setState({
						isLoading: false,
						isRefreshing: false,
					}, () => console.log("loading paused"));
				}, 100)
				setTimeout(() => {
					if (res.code && res.code.code == 401) {
						NavigationService.navigate("Login");
					} else if (res.singleStringMessage) {
						Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
						this.setState({ error: "No Orders Present" });
					} else {
						if (!res.data.orders.length && !orders.length) {
							this.setState({
								loadingMore: false,
								isListEnd: true,
								error: (this.state.first_date && this.state.last_date) ? "No Orders Present" : "Please Select The Date's"
							}, () => console.log("data has been set"))
						} else if (!res.data.orders.length && orders.length) {
							this.setState({
								loadingMore: false,
								isListEnd: true,
							}, () => console.log("data has been set"))
						} else {
							// ---------------- Sample Data ----------------
							// res.data["total_commission"] = "150"
							// res.data.orders.forEach(element => {
							// 	element["commission"] = "50"
							// });
							this.setState({
								isListEnd: false,
								loadingMore: false,
								orders: pageNo === 1 ? res.data.orders : [...orders, ...res.data.orders],
								totalCommission:pageNo === 1 ? res.data.totalDriverCommission:(totalCommission + res.data.totalDriverCommission) ,
							}, () => console.log("data has been set into state"));
						}
					}
				}, 500)
			}).catch((err) => {
				this.setState({ loading: false });
				console.log("login error" + JSON.stringify(err));
			})
		});
	}

	hideDateTimePicker = () => {
		this.setState({
			isFirstDateOpened: false,
			isLastDateOpened: false
		})
	}

	searchOrders = () => {
		if (this.state.first_date > this.state.last_date) {
			alert('Please select the valid date')
		}
		else {
			this.refreshOrder()
		}
	}

	keyExtractor = (item) => item._id;

	handleLoadMore = () => {
		if (!this.state.loadingMore && !this.state.isListEnd) {
			this.setState({
				loadingMore: true,
				pageNo: this.state.pageNo + 1
			}, () => {
				this.getOrderHistory();
			});
		}
	};

	refreshOrder = () => {
		if (!this.state.isLoading && !this.state.isRefreshing) {
			this.setState({
				pageNo: 1,
				isRefreshing: true
			}, () => {
				this.getOrderHistory();
			})
		}
	}

	renderOrderList = () => {
		let { orders, isRefreshing } = this.state;
		return (
			<View style={{ width: '100%', flex: 1 }}>
				<FlatList data={orders}
					renderItem={({ item }) => this.renderOrder(item)}
					keyExtractor={this.keyExtractor}
					onEndReached={this.handleLoadMore}
					onEndReachedThreshold={1}
					onRefresh={this.refreshOrder}
					refreshing={isRefreshing} />

				{this.renderFooter()}
			</View>
		)
	}

	renderOrder = (item) => {
		return (
			<TouchableOpacity style={{ width: "100%", height: "auto" }}
				onPress={() => NavigationService.navigate("OrderDetail", { order: item })}>
				<CardView cardElevation={5}
					cardMaxElevation={5} cornerRadius={5}
					style={styles.productBox}>
					<View style={{
						backgroundColor: Constants.lightgreen,
						width: "100%",
						height: 30,
						alignItems: "center",
						flexDirection: "row",
						paddingLeft: 10
					}}>
						<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Delivered on </Text>
						<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.bold }}>{moment(item.deliver_end_time).format("DD-MMM-YY HH:mm:ss")}</Text>
					</View>
					<View style={{
						height: 70, width: '100%',
						flexDirection: 'row', alignItems: "center",
						justifyContent: "space-between"
					}}>
						<Text style={{
							color: Constants.black,
							fontSize: 18,
							fontFamily: Fonts.bold,
							marginLeft: 12,
						}}>{item.address.delivery.full_name}</Text>
						<View style={{
							height: 40,
							backgroundColor: Constants.lightgreen,
							paddingLeft: 15,
							paddingRight: 5,
							justifyContent: "center",
							alignItems: "center",
							borderTopLeftRadius: 20,
							borderBottomLeftRadius: 20
						}}>
							<Text style={{
								color: Constants.black,
								fontSize: 16, fontFamily: Fonts.semiBold
							}}>{(item.total_amount_after_tax + item.delivery_charges) - item.discount} {Constants.currency}</Text>
						</View>
					</View>
					<View style={{
						height: 50, width: '100%',
						flexDirection: 'row',
					}}>
						<View style={{ height: '100%', width: '33%', }}>
							<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Order ID</Text>
							<Text style={{ fontFamily: Fonts.bold, fontSize: 12, color: Constants.black, marginLeft: 14, textTransform: "uppercase" }}>{item.order_id}</Text>
						</View>
						<View style={{ backgroundColor: 'lightgray', height: 35, width: 1 }}>
						</View>
						<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
							<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Shop </Text>
							<Text style={{
								marginTop: 1, fontFamily: Fonts.bold,
								fontSize: 12, color: Constants.black, textAlign: "center"
							}}>{item.store.name}</Text>
						</View>
						<View style={{ backgroundColor: 'lightgray', height: 35, width: 1 }}>
						</View>
						<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
							<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Payment Status </Text>
							<Text style={{
								marginTop: 1, fontFamily: Fonts.bold,
								fontSize: 12, color: (item.payment_type == 2 || item.status == 3) ? Constants.green : Constants.gold,
								marginLeft: 14
							}}>{(item.payment_type == 2 || item.status == 3) ? "Paid" : "Pending"} </Text>
						</View>
					</View>
					<View style={{ backgroundColor: 'lightgray', height: 1, width: '100%' }}></View>
					<View style={{ flexDirection: 'row', width: '100 %', padding: 10 }}>
						<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Commission </Text>
						<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.bold }}>{(item.driver_commission?item.driver_commission:0) + " " + Constants.currency} </Text>
					</View>
				</CardView>
			</TouchableOpacity>
		)
	}

	renderFooter = () => {
		let { isListEnd, loadingMore } = this.state;
		if (!loadingMore || isListEnd) {
			console.log("loadingMore:-" + loadingMore)
			return null;
		}
		return (
			<View
				style={{
					width: "100%",
					height: 50,
					marginTop: 10,
					marginBottom: 10,
					borderColor: Constants.lightgreen,
					justifyContent: "center",
					alignItems: "center"
				}}
			>
				<ActivityIndicator
					color={Constants.green}
					animating
					size="large" />
			</View>
		);
	}

	render() {
		let { first_date, isFirstDateOpened,
			last_date, isLastDateOpened,
			orders, error, isLoading, totalCommission } = this.state;
		return (
			<View style={{
				flex: 1,
				justifyContent: "flex-start",
				// marginBottom: 50
			}}>
				<Spinner size="large"
					visible={isLoading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<DateTimePicker isVisible={isFirstDateOpened}
					onConfirm={this.getFirstDate}
					onCancel={this.hideDateTimePicker}
					titleIOS="Select First Date"
					maximumDate={new Date()}
					date={first_date}
				/>
				<DateTimePicker isVisible={isLastDateOpened}
					onConfirm={this.getLastDate}
					onCancel={this.hideDateTimePicker}
					titleIOS="Select To Date"
					maximumDate={new Date()}
					date={last_date}
				/>
				<View style={{
					width: "100%", height: 80,
					alignItems: "center",
					justifyContent: "center", flexDirection: "row"
				}}>
					<TouchableOpacity style={styles.dateButtons} onPress={() => this.showFirstDate()}>
						<Text style={{ fontSize: 16, color: "white", fontFamily: Fonts.bold }}>
							{
								this.state.first_date ?
									moment(this.state.first_date).format("DD-MMM-YY") :
									"From Date"
							}
						</Text>
						<Image source={Images.ImgCalendarWhite} style={{ width: 20, height: 20 }} resizeMode="contain"></Image>
					</TouchableOpacity>
					<TouchableOpacity style={styles.dateButtons} onPress={() => this.showLastDate()}>
						<Text style={{ fontSize: 16, color: "white", fontFamily: Fonts.bold }}>
							{
								this.state.last_date ?
									moment(this.state.last_date).format("DD-MMM-YY") :
									"To Date"
							}
						</Text>
						<Image source={Images.ImgCalendarWhite} style={{ width: 20, height: 20 }} resizeMode="contain"></Image>
					</TouchableOpacity>
					<TouchableOpacity style={styles.searchButton} onPress={() => this.searchOrders()}>
						<Image source={Images.ImgSearch} style={{ width: 20, height: 20 }} resizeMode="contain"></Image>
					</TouchableOpacity>
				</View>
				{error ?
					<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
						<Text>{error}</Text>
					</View> :

					<View style={{ width: '100%', flex: 1 }}>
						{this.renderOrderList()}
						<View style={{
							backgroundColor: Constants.lightgreen,
							width: "100%",
							padding: 10
						}}>
							<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>Total Commission </Text>
						</View>
						<View style={{
							flexDirection: 'row',
							// backgroundColor: Constants.lightgreen,
							width: "100%",
							justifyContent: "space-between",
							paddingLeft: 10,
							paddingVertical:10,
						}}>
							<View style={{
								flexDirection: 'row',
								alignItems:'center'
							}}>
								<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>From </Text>
								<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.bold }}>{moment(first_date).format("DD-MMM-YY")}</Text>
								<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium }}>   To </Text>
								<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.bold }}>{moment(last_date).format("DD-MMM-YY")} </Text>
							</View>

							<View style={{
								height: 40,
								backgroundColor: Constants.lightgreen,
								paddingLeft: 15,
								paddingRight:5,
								justifyContent: "center",
								alignItems: "center",
								borderTopLeftRadius: 20,
								borderBottomLeftRadius: 20,
								flexDirection: 'column',
							}}>
								<Text style={{
									color: Constants.black,
									fontSize: 16, fontFamily: Fonts.semiBold
								}}>{totalCommission} {Constants.currency}</Text>
							</View>
						</View>
					</View>

				}
				{/* <SafeAreaView style={{ backgroundColor: Constants.green }}></SafeAreaView> */}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	dateButtons: {
		backgroundColor: Constants.green,
		height: 40,
		width: '37%',
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingLeft: 13,
		paddingRight: 13,
		borderRadius: 25,
		margin: 5
	},
	searchButton: {
		backgroundColor: Constants.green,
		height: 40,
		width: 40,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 25,
		marginLeft: 10
	},
	bars: {
		height: 40,
		width: "100%",
		justifyContent: "center",
		paddingLeft: 15, marginTop: 10
	},
	Header: {
		width: '100%',
		height: 200,
		alignItems: 'center'
	},
	productBox: {
		flex: 1,
		marginTop: 10,
		backgroundColor: "white",
		// height: 150,
		width: '95%',
		borderRadius: 10,
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: .8,
		shadowRadius: 2,
		elevation: 1,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: '2.5%',
		marginBottom: 10
	},
	headerlabel: {
		marginTop: 50, fontSize: 16,
		color: Constants.white, fontFamily: Fonts.bold
	}
});