import { StyleSheet } from 'react-native';
import Constants from '../../Componants/Constants';
import Fonts from '../../Componants/Fonts';

export default styles = StyleSheet.create({
	orderBox: {
		marginTop: 20,
		backgroundColor: "white",
		width: '100%',
		borderRadius: 10,
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: .8,
		shadowRadius: 2,
		elevation: 1,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: '2.5%',
		marginBottom: 10
	},
	expressBox: {
		width: '75%',
		height: 30,
		minWidth: 220,
		backgroundColor: 'white',
		left: '12%',
		position: "absolute",
		top: -15,
		zIndex: 1,
		borderColor: 'lightgray',
		borderRadius: 5, borderWidth: 1,
		flexDirection: 'row'
	},
	expressLabelView: { width: '50%', alignItems: 'center', justifyContent: 'center' },
	lateImage: { width: 10, height: 20, marginLeft: 5, marginTop: 5 },
	lateView: {
		width: '40%',
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'row',
		paddingLeft: 5
	},
	latetext: {
		color: Constants.red,
		fontSize: 16, fontFamily: Fonts.bold
	},
	lateText1: { color: Constants.black, fontSize: 10, fontFamily: Fonts.regular },
	NotLateView: {
		width: '25%',
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'row',
		paddingLeft: 5
	},
	NotLateViewText: {
		color: Constants.black,
		fontSize: 16, fontFamily: Fonts.bold
	},
	firstView: { height: 85, width: '100%', flexDirection: 'row' },
	firstInfoView: {
		height: '100%',
		width: '65%', flex: 1,
		flexDirection: 'row',
		paddingLeft: 7
	},
	imageView: {
		width: "30%",
		height: 60, borderRadius: 30,
		justifyContent: "center",
		alignItems: "center",
		marginTop: 15,
	},
	firstInfoImage: {
		width: "100%", height: "100%",
		borderRadius: 30,
		resizeMode: "cover"
	},
	callingButton: {
		width: 30, height: 30, zIndex: 10,
		position: 'absolute', top: -8, right: -10
	},
	actionButton: {
		height: 30, alignItems: 'center',
		// backgroundColor: Constants.green,
		width: '35%', marginRight: 7,
		borderRadius: 15, marginTop: 30
	},
	actionButtonText: {
		fontSize: 11, color: 'white',
		marginTop: 8, fontFamily: Fonts.bold
	},
	secondPanel: {
		height: 80, width: '100%',
		alignItems: "center",
		flexDirection: 'row',
	},
	secondPanelParts: {
		height: '100%', width: '33%',
		alignItems: 'center', justifyContent: "center"
	},
	secondpanelPartInfo: {
		fontSize: 12,
		color: Constants.black,
		fontFamily: Fonts.medium, textAlign: "center"
	},
	secondPanelVerticalBorder: { backgroundColor: 'lightgray', height: '100%', width: 1 },
	thirdPanel: {
		borderTopWidth: 1,
		borderTopColor: "lightgray",
		marginTop: 10,
		width: '100%', paddingTop: 10, paddingBottom: 10,
		flexDirection: 'row', alignItems: "center",
	},
	thirdPanelView: {
		width: '80%',
		flex: 1, flexDirection: 'row',
		alignItems: "center",
		justifyContent: "space-around",
	},
	pickerButton: {
		width: "10%", maxWidth: 30,
		alignItems: "center",
		justifyContent: "center",
	},
	pickerButtonView: {
		width: '65%',
		flexDirection: 'row', alignItems: "center",
	},
	pickerButtonText: {
		color: Constants.black,
		fontSize: 14,
		width: "80%", fontFamily: Fonts.medium
	},
	priceView: {
		width: '20%',
		justifyContent: "center",
	},
	priceViewText: {
		fontFamily: Fonts.bold,
		color: Constants.black, fontSize: 14
	}
})