import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import Images from '../../../Componants/Images';
import Constants from '../../../Componants/Constants';
import CardView from 'react-native-cardview';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import * as ApiManager from './../../../ApiManager/ApiManager';
import moment from 'moment';
import Fonts from '../../../Componants/Fonts';
import Toast from 'react-native-simple-toast';
import NavigationService from '../../../Componants/NavigationService';

export default class ScheduledOrder extends Component {
	constructor(props) {
		super(props);
		this.state = {
			orders: [],
			loading: false
		};
	}

	static navigationOptions = {
		title: 'Scheduled Orders',
		headerStyle: {
			backgroundColor: Constants.green,
		},
		headerTintColor: '#fff',
		headerTitleStyle: {
			fontFamily: Fonts.bold,
			fontSize: 17,
			marginBottom: 10
		},
	};

	getOrders = () => {
		if (!Constants.is_available) {
			Toast.showWithGravity("Sorry, you are offline", Toast.SHORT, Toast.BOTTOM);
			return
		}
		this.setState({ loading: true });
		ApiManager.getScheduledOrders().then((res) => {
			setTimeout(() => {
				this.setState({ loading: false }, () => console.log("loading paused"));
			}, 100)
			setTimeout(() => {
				if (res.code && res.code.code == 401) {
					this.props.navigation.navigate("Login");
				}
				if (res.singleStringMessage) {
					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.BOTTOM);
				} else {
					this.setState({ orders: res.data.orders }, () => {
						// NavigationService.navigate("OrderDetail", { order: res.data.orders[0] });
						console.log("data has been set into state")
					});
				}
			}, 500);
		}).catch(() => {
			this.setState({ loading: false }, () => console.log("loading paused"));
		});
	}

	render() {
		return (
			<View style={{
				flex: 1,
				justifyContent: "flex-start",
				backgroundColor: Constants.backgroundPageColor
			}}>
				<NavigationEvents
					onWillFocus={() => {
						console.log(this.props.navigation.state.routeName);
						this.getOrders();
						//Call whatever logic or dispatch redux actions and update the screen!
					}}
				/>
				<Spinner
					size="large"
					visible={this.state.loading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<ScrollView style={{ flex: 1 }}>
					{
						this.state.orders.map((totalOrder, index) =>
							(
								<View key={index} style={{ width: "100%", height: "auto" }}>
									{
										(!(this.state.orders[index - 1]) || (totalOrder.order_date != this.state.orders[index - 1].order_date)) ?
											<View style={styles.bars}>
												<Text style={{ fontFamily: Fonts.bold, fontSize: 16, color: Constants.black }}>
													{totalOrder.order_date}
												</Text>
											</View> : null
									}
									<CardView key={index}
										cardElevation={5}
										cardMaxElevation={5}
										cornerRadius={5}
										style={[styles.productBox, { backgroundColor: "white" }]} >
										<View style={{ height: '40%', width: '100%', flexDirection: 'row' }}>
											<View style={{
												width: 60, height: 60, borderRadius: 30,
												borderColor: 'lightgray',
												borderWidth: 5,
												marginTop: 10, marginLeft: 10,
												justifyContent: "center",
												alignItems: "center"
											}}>
												{
													(totalOrder.customer && totalOrder.customer.picture) ?
														<Image style={{ width: 56, height: 56, borderRadius: 30, }}
															resizeMode="cover" source={{ uri: totalOrder.customer.picture }} /> :
														<Image style={{ width: 56, height: 56, borderRadius: 30, }} resizeMode="cover" source={Images.ImgSunny} />
												}
											</View>
											<View style={{ marginTop: 25, marginLeft: 15 }}>
												<Text style={{ fontFamily: Fonts.bold, color: Constants.black, fontSize: 16 }}>{totalOrder.address.delivery.full_name}</Text>
												<View style={{
													flexDirection: "row"
												}}>
													<Image source={Images.ImgClock} style={{ width: 12, height: 12, margin: 3, marginTop: 1 }}></Image>
													<Text style={{ color: 'gray', fontSize: 12, fontFamily: Fonts.medium }}>{moment(totalOrder.deliver_start_time).format("ha ")}-
																{moment(totalOrder.deliver_end_time).format(" ha")}</Text>
												</View>
											</View>
										</View>
										<View style={{ height: '25%', width: '100%', flexDirection: 'row', marginTop: 10 }}>
											<TouchableOpacity onPress={() => NavigationService.navigate("OrderDetail", { order: totalOrder })} style={{ height: '100%', width: '33%', alignItems: "center" }}>
												<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Order ID</Text>
												<Text style={{ fontFamily: Fonts.bold, fontSize: 12, color: Constants.black, marginLeft: 14, textTransform: "uppercase" }}>#{totalOrder.order_id}</Text>
											</TouchableOpacity>
											<View style={{ backgroundColor: 'lightgray', height: '100%', width: 1 }}>
											</View>
											<View style={{ height: '100%', width: '33%', alignItems: 'center' }}>
												<Text style={{ fontSize: 12, marginLeft: 14, color: Constants.black, fontFamily: Fonts.medium }}>Shop </Text>
												<Text style={{
													marginTop: 1, fontFamily: Fonts.bold,
													fontSize: 12, color: Constants.black, textAlign: "center"
												}}>{totalOrder.store ? totalOrder.store.name : "N/A"}</Text>
											</View>
											<View style={{ backgroundColor: 'lightgray', height: '100%', width: 1 }}>
											</View>
											<View style={{ height: '100%', width: '33%', alignItems: 'center', }}>
												<Text style={{ fontSize: 12, color: Constants.black, fontFamily: Fonts.medium, }}>Payment Status </Text>
												<Text style={{
													marginTop: 1, fontFamily: Fonts.bold,
													fontSize: 12, color: totalOrder.payment_type == 2 ? Constants.green : Constants.gold,
													// marginLeft: 14
												}}>{totalOrder.payment_type == 1 ? "Pending" : "Paid"} </Text>
											</View>
										</View>
										<View style={{ backgroundColor: 'lightgray', height: 1, width: '100%', marginTop: 10 }}>
										</View>
										<View style={{
											height: '25%',
											width: '100%', flexDirection: 'row',
											justifyContent: "space-between",
										}}>
											<View style={{
												width: "80%",
												height: "100%",
												flexDirection: "row",
												alignItems: "center",
												justifyContent: "space-around",
											}}>
												<View style={{ width: "20%", minWidth: 30, maxWidth: 30, height: "100%", alignItems: "center", justifyContent: "center" }}>
													<Image style={{ width: 18, height: 18 }} resizeMode="cover" source={Images.ImgLocation} />
												</View>
												<View style={{
													width: 'auto', height: "100%",
													flexDirection: 'row', alignItems: "center",
												}}>
													<Text style={{
														color: Constants.black,
														fontSize: 14,
														width: "80%", fontFamily: Fonts.medium
													}} numberOfLines={2}>{totalOrder.address.delivery.house_no} {totalOrder.address.delivery.landmark} {totalOrder.address.delivery.locality}</Text>
												</View>
											</View>
											<View style={{
												width: "20%",
												height: "100%",
												justifyContent: "center", paddingRight: 7
											}}>
												<Text style={{
													fontFamily: Fonts.bold, textAlign: "center",
													color: Constants.black, fontSize: 14
												}}>{Constants.currency} {(totalOrder.total_amount_after_tax + totalOrder.delivery_charges) - totalOrder.discount}</Text>
											</View>
										</View>
									</CardView>
								</View>
							))
					}
				</ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	bars: {
		height: 40,
		width: "100%",
		justifyContent: "center",
		paddingLeft: 15, marginTop: 10
	},
	Header: {
		width: '100%',
		height: 200,
		alignItems: 'center'
	},
	productBox: {
		marginTop: 10,
		height: 200,
		width: '90%',
		borderRadius: 10,
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: .8,
		shadowRadius: 2,
		elevation: 1,
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: '5%',
		marginRight: '5%',
		marginBottom: 10
	},
	headerlabel: {
		marginTop: 50, fontSize: 16,
		color: Constants.white, fontFamily: Fonts.bold
	}
});