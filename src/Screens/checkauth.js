import React, { Component } from 'react';
import { ActivityIndicator, StatusBar, StyleSheet, ImageBackground, Text, DeviceEventEmitter } from 'react-native';
import Images from '../Componants/Images';
import constants from '../Componants/Constants';
import Fonts from '../Componants/Fonts';
import SInfo from 'react-native-sensitive-info';
import firebase from 'react-native-firebase';
import Constants from '../Componants/Constants';
import NavigationService from '../Componants/NavigationService';
import * as Actions from '../Redux/Action/Index';
import { connect } from 'react-redux';

class AuthScreen extends Component {

	componentWillMount = async () => {
		// await SInfo.deleteItem('token', constants.sharedPreference);
		// await SInfo.deleteItem('userdetail', constants.sharedPreference);
		// await SInfo.deleteItem('fcmToken', constants.sharedPreference);
		const token = await SInfo.getItem('token', constants.sharedPreference);
		const user = await SInfo.getItem('userdetail', constants.sharedPreference);
		const fcmToken = await SInfo.getItem('fcmToken', constants.sharedPreference);
		const is_online = await SInfo.getItem('is_online', constants.sharedPreference);
		constants.user = user ? JSON.parse(user) : {};
		constants.TOKEN = token ? token : "";
		constants.is_available = is_online ? true : false;
		this.props.saveToken(token ? token : "");
		this.props.saveUserData(user ? JSON.parse(user) : {})
		if (fcmToken) {
			constants.FCM_TOKEN = fcmToken;
			this.props.saveFcmToken(fcmToken);
			console.log("routing decieded");
			NavigationService.navigate(token ? 'Main' : 'Login');
		} else {
			this.checkPermission();
		}
	}

	checkPermission = () => {
		firebase.messaging().hasPermission().then(enabled => {
			if (enabled) {
				this.getToken();
			} else {
				this.requestPermission();
			}
		});
	}

	//3
	getToken = () => {
		firebase.messaging().getToken().then(async (fcmToken) => {
			if (fcmToken) {
				// user has a device token
				console.log("fcm id is :--" + fcmToken);
				this.props.saveFcmToken(fcmToken);
				constants.FCM_TOKEN = fcmToken;
				await SInfo.setItem('fcmToken', fcmToken, Constants.sharedPreference);
				NavigationService.navigate(constants.TOKEN ? 'Main' : 'Login');
				// this.props.navigation.navigate(constants.TOKEN ? 'Main' : 'Login');
			}
		}).catch(err => {
			throw new Error(err)
		})
	}

	//2
	requestPermission = () => {
		firebase.messaging().requestPermission()
			.then(() => {
				this.getToken();
			})
			.catch(error => {
				NavigationService.navigate('Login');
				console.log("User has rejected permission");
			});
	}

	// Render any loading content that you like here
	render() {
		console.log("checking auth");
		console.disableYellowBox = true;
		return (
			<ImageBackground source={Images.ImgSplash} style={styles.container} resizeMode="stretch">
				<ActivityIndicator color={constants.green} size="large"></ActivityIndicator>
				<Text style={styles.text}>Loading . . .</Text>
				<StatusBar backgroundColor="transparent" hidden={true} barStyle="light-content" />
			</ImageBackground>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	logo: {
		width: "75%",
		maxWidth: 150
	},
	text: {
		color: "white",
		fontSize: 18,
		fontFamily: Fonts.bold,
		marginTop: 20
	}
});

const mapDispatchToProps = (dispatch) => {
	return {
		saveToken: (data) => dispatch(Actions.saveToken(data)),
		saveUserData: (data) => dispatch(Actions.saveDriverData(data)),
		saveFcmToken: (data) => dispatch(Actions.saveFcmToken(data)),
	}
}

export default connect(null, mapDispatchToProps)(AuthScreen)