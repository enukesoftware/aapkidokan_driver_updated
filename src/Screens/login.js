import React, { Component } from 'react';
import { View, StyleSheet, Image, DeviceEventEmitter, Text, TextInput, TouchableOpacity, ImageBackground } from 'react-native';
import Images from '../Componants/Images';
import Constants from '../Componants/Constants';
import Toast from 'react-native-simple-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import { NavigationEvents } from "react-navigation";
import * as ApiManager from './../ApiManager/ApiManager';
import SInfo from 'react-native-sensitive-info';
import Fonts from '../Componants/Fonts';
import NetInfo from "@react-native-community/netinfo";
import firebase from 'react-native-firebase';
import NavigationService from '../Componants/NavigationService';
import { connect } from 'react-redux';
import * as Actions from '../Redux/Action/Index';

class LogInScreen extends Component {
	static navigationOptions = {
		// title: 'Please sign in',
		header: null //hide the header
	};

	constructor(props) {
		super(props)
		this.state = {
			// username: 'khushal.pahuja@enukesoftware.com',
			// password: '1234567',
			username: '',
			password: '',
			loading: false
		}
	}

	componentDidMount = () => {
		this.checkPermission();
	}

	//1
	checkPermission = () => {
		firebase.messaging().hasPermission().then(enabled => {
			if (enabled) {
				this.getToken();
			} else {
				this.requestPermission();
			}
		});
	}

	//3
	getToken = () => {
		firebase.messaging().getToken().then(async (fcmToken) => {
			if (fcmToken) {
				// user has a device token
				console.log("fcm id is :--" + fcmToken);
				Constants.FCM_TOKEN = fcmToken;
				await SInfo.setItem('fcmToken', fcmToken, Constants.sharedPreference);
				this.signIn();
				// NavigationService.navigate(Constants.TOKEN ? 'Main' : 'Login');
			}
		}).catch(err => {
			throw new Error(err);
			this.signIn();
		})
	}

	//2
	requestPermission = () => {
		firebase.messaging().requestPermission()
			.then(() => {
				this.getToken();
			})
			.catch(error => {
				this.signIn();
				// NavigationService.navigate('Login');
				console.log("User has rejected permission");
			});
	}

	signIn = async () => {
		let emailcontext = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (!this.state.username || !this.state.username.length) {
			Toast.showWithGravity("Please enter your email or Mobile no", Toast.SHORT, Toast.BOTTOM)
		} else {
			if (isNaN(this.state.username) && !emailcontext.test(this.state.username)) {
				Toast.showWithGravity("Please enter valid email ID", Toast.SHORT, Toast.BOTTOM)
			} else if (!isNaN(this.state.username) && ((this.state.username.startsWith('0') && (this.state.username.length != 11)) || (!this.state.username.startsWith('0') && (this.state.username.length != 10)))) {
				Toast.showWithGravity("Please enter valid Mobile no.", Toast.SHORT, Toast.BOTTOM)
			} else if (!this.state.password || !this.state.password.length) {
				Toast.showWithGravity("Please enter your password.", Toast.SHORT, Toast.BOTTOM)
			} else {
				const netInfo = await NetInfo.fetch();
				if (!netInfo.isConnected) {
					Toast.showWithGravity("Internet is not available", Toast.LONG, Toast.BOTTOM);
					return
				}
				this.setState({ loading: true });
				let body = { username: this.state.username, password: this.state.password };
				ApiManager.login(body).then((res) => {
					setTimeout(() => {
						this.setState({ loading: false });
					}, 100);
					if (res && res.success && res.code == 200) {
						setTimeout(() => {
							Toast.showWithGravity(
								'Login Successful',
								Toast.LONG,
								Toast.BOTTOM //TOP,BOTTOM
							);
							if (res.data.user.picture && !res.data.user.picture.startsWith("http")) {
								res.data.user.picture = Constants.imageBaseURL + res.data.user.picture;
							}
							Constants.userToken = res.data.token;
							Constants.user = res.data.user;
							SInfo.setItem('token', res.data.token + '', Constants.sharedPreference);
							SInfo.setItem('userdetail', JSON.stringify(res.data.user), Constants.sharedPreference);
							this.props.saveDriverData(res.data.user);
							DeviceEventEmitter.emit(Constants.WATCH_LOCATION);
							NavigationService.navigate('Home');
						}, 500);
					} else {
						setTimeout(() => {
							Toast.showWithGravity(
								(res && res.singleStringMessage) ? res.singleStringMessage : "Unknown Error",
								Toast.LONG,
								Toast.BOTTOM //TOP,BOTTOM
							);
						}, 500);
					}
					console.log(res)
					// });
				}).catch((err) => {
					this.setState({ loading: false });
					console.log("login error" + JSON.stringify(err));
				})
			};
		}
	}

	render() {
		console.log("Login");
		return (
			<ImageBackground source={Images.ImgBackground} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<Spinner
					size="large"
					visible={this.state.loading}
					color={Constants.green}
					textContent={'Please wait . . .'}
					textStyle={{ color: "white", fontFamily: Fonts.bold }}
				/>
				<NavigationEvents
					onWillFocus={() => {
						this.setState({ username: "", password: "" });
						console.log("it will called allways")
						//Call whatever logic or dispatch redux actions and update the screen!
					}}
				/>
				<View style={[styles.innerSection]}>
					<View style={styles.logoSection}>
						<Image source={Images.ImgLogo} style={styles.logo}></Image>
					</View>
					<Text style={[styles.loginlabel]}>Log In</Text>
					<View style={[styles.inputSection]}>
						<Image source={Images.ImgUserIcon} style={styles.inputLogo} />
						<TextInput
							style={styles.input}
							placeholder={"Email Id or Mobile Number"}
							placeholderTextColor={Constants.placeHolderColor}
							value={this.state.username}
							// keyboardType="decimal-pad"
							onChangeText={(username) => this.setState({ username })}
						/>
					</View>
					<View style={[styles.inputSection]}>
						<Image source={Images.ImgPassword} style={styles.inputLogo} />
						<TextInput
							style={styles.input}
							placeholder={"Password"}
							placeholderTextColor={Constants.placeHolderColor}
							value={this.state.password}
							keyboardType="default"
							secureTextEntry={true}
							onChangeText={(password) => this.setState({ password })}
							returnKeyType="done"
							onSubmitEditing={() => this.checkPermission()}
						/>
					</View>
					<TouchableOpacity style={styles.loginButton}
						onPress={() => this.checkPermission()}>
						<Text style={[styles.loginText]}>LOGIN</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.forgotLine}
						onPress={() => this.props.navigation.navigate("ForgotPassword")}>
						<Text style={{ fontFamily: Fonts.regular }}>Forgot your password?</Text>
						<Text style={{ fontFamily: Fonts.bold, marginLeft: 5 }}>Reset it</Text>
					</TouchableOpacity>
				</View>
			</ImageBackground>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#2c3e50',
		justifyContent: 'center'
	},
	input: {
		width: "80%",
	},
	loginContainer: {
		width: '100%',
		height: '100%',
	},
	logo: {
		width: '100%',
		height: '100%'
	},
	formContainer: {
		padding: 20
	},
	buttonContainer: {
		backgroundColor: '#2980b6',
		paddingVertical: 15,
		marginBottom: 5
	},
	innerSection: {
		backgroundColor: 'white',
		alignItems: 'center',
		shadowOpacity: 1,
		shadowRadius: 3,
		shadowOffset: { height: 0, width: 0 },
		elevation: 1, zIndex: 0, borderRadius: 10,
		width: '95%',
		minHeight: 280,
	},
	logoSection: {
		width: 120,
		height: 115,
		position: "absolute",
		zIndex: 2,
		backgroundColor: Constants.green,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 65,
		top: -60
	},
	logo: { width: 80, height: 50 },
	loginlabel: {
		marginTop: 70, fontSize: 22,
		color: Constants.black, fontFamily: Fonts.bold
	},
	inputSection: {
		borderWidth: 1,
		borderColor: 'lightgrey',
		borderRadius: 25,
		width: '85%',
		height: 50,
		flexDirection: "row",
		marginTop: 10,
		marginBottom: 5
	},
	inputLogo: {
		width: 20,
		height: 20,
		resizeMode: 'contain',
		marginTop: 14,
		marginLeft: 15,
		marginRight: 15
	},
	loginButton: {
		borderWidth: 1,
		minWidth: '85%',
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 20,
		marginBottom: 15,
		borderRadius: 25,
		backgroundColor: Constants.green,
		borderColor: Constants.green
	},
	forgotLine: {
		width: "auto",
		height: 20,
		alignContent: "flex-end",
		alignItems: "center",
		flexDirection: "row",
		color: '#212121',
		marginBottom: 50
	},
	loginText: {
		fontSize: 22,
		color: Constants.white,
		fontFamily: Fonts.bold,
	},
	buttonText: {
		color: '#fff',
		textAlign: 'center',
		fontFamily: Fonts.bold
	}
});

const mapDispatchToProps = (dispatch) => {
	return {
		saveDriverData: (data) => dispatch(Actions.saveDriverData(data)),
		saveToken: (data) => dispatch(Actions.saveToken(data)),
	}
}

export default connect(null, mapDispatchToProps)(LogInScreen)